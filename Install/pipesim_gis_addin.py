import arcpy
import json
import pythonaddins
import sys
import hashlib
from subprocess import Popen
from os import path, remove
from collections import OrderedDict, defaultdict

sys.path.append(path.dirname(__file__))
try:
    from helper import DataStore, create_fields_if_not_exist, PipeSimException, update_connection, find_end_points, get_unique, create_feature_layer, CursorWithEditor, create_geo_database, export_rlf, get_connected_pipes, get_layer_OID
    import point_ui
    import pipe_ui
    import settings_ui
    import run_ui
    import cal_ui
    import licence_ui
    import pipesim_format_select_ui
    from pipe_steady_api import calculate_pipeline_in_sub_process
    from pipe_steady_client import run_pipeline, get_result_pipeline, terminate_job
    from translation import Language
except ImportError as e:
    pythonaddins.MessageBox(e.message, "ImportError")
LANG = Language()


def is_licence_valid():
    dir_name, file_name = path.split(path.abspath(__file__))

    try:
        try:
            licence = json.load(open(dir_name + '\\licence.json'))
        except Exception:
            licence = json.loads(licence_ui.exec_in_sub_process())

        unique = get_unique()
        salt = unique + 'PIPESIM_GIS_SECRET_CODE_A'
        key = hashlib.sha224(salt).hexdigest()
        if key == licence['key']:
            with open(dir_name + '\\licence.json', 'w') as outfile:
                json.dump(licence, outfile)
            return True
    except Exception:
        pass
    try:
        remove(dir_name + '\\licence.json')
    except Exception:
        pass
    return False


class ConfigurePoint(object):
    """Implementation for pipesim_gis_addin.tool (Tool)"""
    def __init__(self):
        self.enabled = True
        self.shape = "NONE"

    def onMouseUpMap(self, x, y, button, shift):
        try:
            lyr = DataStore.instance().point_layer
            DataStore.instance().select_shape_by_location(lyr, x, y)
            fields_name = ["OID@"] + [f['name'] for _, f in DataStore.instance().settings['point_fields'].iteritems()]
            create_fields_if_not_exist(lyr, DataStore.instance().settings['point_fields'].values())
            with arcpy.da.SearchCursor(lyr, fields_name) as cursor:
                row = next(cursor, None)
                if row is None:
                    raise PipeSimException(LANG.empty_selection)
                point = {k: v for k, v in zip(DataStore.instance().settings['point_fields'], row[1:])}
                point['OID@'] = row[0]

            try:
                point_new = json.loads(point_ui.exec_in_sub_process({
                    'point': point,
                    'units': DataStore.instance().settings['units']
                }))
            except ValueError:
                return
            with CursorWithEditor(arcpy.da.UpdateCursor, lyr, fields_name) as cursor:
                row = next(cursor)
                for i, (f, opt) in enumerate(DataStore.instance().settings['point_fields'].iteritems()):
                    if point_new.get(f) is not None:
                        try:
                            if opt['db_attrs']['field_type'] == 'FLOAT':
                                row[i + 1] = float(point_new[f])
                            else:
                                row[i + 1] = int(point_new[f])
                        except Exception:
                            pass
                cursor.updateRow(row)

        except PipeSimException as e:
            pythonaddins.MessageBox(e.message, "Some thing went wrong!", 0)


class ConfigurePipe(object):
    """Implementation for pipesim_gis_addin.tool2 (Tool)"""
    def __init__(self):
        self.enabled = True
        self.shape = "NONE"

    def onMouseUpMap(self, x, y, button, shift):
        try:
            lyr = DataStore.instance().pipe_layer
            DataStore.instance().select_shape_by_location(lyr, x, y)
            fields_name = ["OID@"] + [f['name'] for _, f in DataStore.instance().settings['pipe_fields'].iteritems()]
            create_fields_if_not_exist(lyr, DataStore.instance().settings['pipe_fields'].values())
            with arcpy.da.SearchCursor(lyr, fields_name) as cursor:
                row = next(cursor, None)
                if row is None:
                    raise PipeSimException(LANG.empty_selection)
                pipe = {k: v for k, v in zip(DataStore.instance().settings['pipe_fields'], row[1:])}
                pipe['OID@'] = row[0]

            response = pipe_ui.exec_in_sub_process({
                'pipe': pipe,
                'units': DataStore.instance().settings['units'],
                'npses': DataStore.instance().settings['use_nps'] and [p[0] for p in DataStore.instance().settings['nps_dict']]
            })
            if response == "update_connection":
                with arcpy.da.SearchCursor(lyr, ['SHAPE@']) as cursor:
                    row = next(cursor)
                    pipe_shape = row[0]
                pipe_new = pipe
                pipe_new['start'], pipe_new['end'] = update_connection(pipe_shape)
                pipe_new['length'] = pipe_shape.length
            else:
                try:
                    pipe_new = json.loads(response)
                except ValueError:
                    return
            with CursorWithEditor(arcpy.da.UpdateCursor, lyr, fields_name) as cursor:
                row = next(cursor)
                for i, (f, opt) in enumerate(DataStore.instance().settings['pipe_fields'].iteritems()):
                    if pipe_new.get(f) is not None:
                        try:
                            if opt['db_attrs']['field_type'] == 'FLOAT':
                                row[i + 1] = float(pipe_new[f])
                            else:
                                row[i + 1] = int(pipe_new[f])
                        except Exception:
                            pass
                cursor.updateRow(row)

            if response == "update_connection":
                self.onMouseUpMap(x, y, button, shift)

        except PipeSimException as e:
            pythonaddins.MessageBox(e.message, "Some thing went wrong!", 0)


class CheckPipeline(object):
    """Implementation for pipesim_gis_addin.button2 (Button)"""
    def __init__(self):
        self.enabled = True
        self.checked = False

    def onClick(self):
        try:
            if is_licence_valid() is False:
                raise PipeSimException(LANG.invalid_licence)

            pipe_lyr = DataStore.instance().pipe_layer
            point_lyr = DataStore.instance().point_layer
            arcpy.SelectLayerByAttribute_management(pipe_lyr, "CLEAR_SELECTION")
            arcpy.SelectLayerByAttribute_management(point_lyr, "CLEAR_SELECTION")
            dir_name, _ = path.split(point_lyr.dataSource)
            join_lyr = dir_name + "\\join_layer"
            pipes_dict = defaultdict(list)
            end_points = set()

            double_end_free_pipes = []
            one_end_free_pipes = []
            start_free_pipes = []
            end_free_pipes = []
            without_diameter_pipes = []
            free_point = []
            without_data_points = []
            with pythonaddins.ProgressDialog as dialog:
                dialog.title = "Checking Pipeline"
                dialog.description = "checking pipeline ..."
                dialog.animation = "Spiral"
                dialog.progress = 5
                try:
                    arcpy.SpatialJoin_analysis(pipe_lyr, point_lyr, join_lyr, 'JOIN_ONE_TO_MANY', 'KEEP_ALL', search_radius="1 Feet")
                    dialog.progress = 15
                    for row in arcpy.da.SearchCursor(join_lyr, ['join_count', 'TARGET_FID', 'JOIN_FID']):
                        if row[0] == 0:
                            double_end_free_pipes.append(row[1])
                        else:
                            pipes_dict[row[1]].append(row[2])
                    for pipe_id, points in pipes_dict.iteritems():
                        if len(points) < 2:
                            one_end_free_pipes.append(pipe_id)
                except Exception as e:
                    raise PipeSimException(e.message)
                finally:
                    arcpy.Delete_management("join_layer")
                    arcpy.Delete_management(join_lyr)
                dialog.progress = 30

                fields = [
                    'OID@',
                    'SHAPE@',
                    DataStore.instance().settings['pipe_fields']['start']['name'],
                    DataStore.instance().settings['pipe_fields']['end']['name'],
                    DataStore.instance().settings['pipe_fields']['length']['name'],
                    DataStore.instance().settings['pipe_fields']['diameter']['name'],
                    DataStore.instance().settings['pipe_fields']['is_active']['name'],
                ]
                with CursorWithEditor(arcpy.da.UpdateCursor, pipe_lyr, fields) as cursor:
                    for i, row in enumerate(cursor):
                        from_point_id, to_point_id = find_end_points(row[1], pipes_dict[row[0]])
                        row[4] = row[1].length
                        if row[5] is None:
                            without_diameter_pipes.append(str(row[0]))
                        if row[6] is None:
                            row[6] = 1
                        if from_point_id != -1:
                            row[2] = from_point_id
                            end_points.add(from_point_id)
                        else:
                            start_free_pipes.append(row[0])

                        if to_point_id != -1:
                            row[3] = to_point_id
                            end_points.add(to_point_id)
                        else:
                            end_free_pipes.append(row[0])

                        cursor.updateRow(row)
                dialog.progress = 50
                fields = [
                    'OID@',
                    DataStore.instance().settings['point_fields']['consumption']['name'],
                    DataStore.instance().settings['point_fields']['is_source']['name'],
                    DataStore.instance().settings['point_fields']['pressure']['name'],
                    DataStore.instance().settings['point_fields']['is_active_point']['name'],
                    DataStore.instance().settings['point_fields']['is_special']['name'],
                    DataStore.instance().settings['point_fields']['gen_limit']['name'],
                ]
                with CursorWithEditor(arcpy.da.UpdateCursor, point_lyr, fields) as cursor:
                    for i, row in enumerate(cursor):
                        if row[0] not in end_points:
                            free_point.append(str(row[0]))
                        if row[1] is None or row[2] is None:
                            without_data_points.append(str(row[0]))
                        if row[2] is True and (row[3] is None or row[3] == 0):
                            without_data_points.append(str(row[0]))

                        changed = False
                        if row[4] is None:
                            row[4] = 1
                            changed = True
                        if row[3] is None:
                            row[3] = 0
                            changed = True
                        if row[5] is None:
                            row[5] = 0
                            changed = True
                        if row[6] is None:
                            row[6] = 0
                            changed = True
                        if changed:
                            cursor.updateRow(row)
                dialog.progress = 75

                problems = []
                problems += [{'lyr': pipe_lyr, 'id': p, 'mes': LANG.no_point_at_both_ends} for p in double_end_free_pipes]
                problems += [{'lyr': pipe_lyr, 'id': p, 'mes': LANG.point_num_not_2} for p in one_end_free_pipes]
                problems += [{'lyr': pipe_lyr, 'id': p, 'mes': LANG.no_point_at_start} for p in start_free_pipes]
                problems += [{'lyr': pipe_lyr, 'id': p, 'mes': LANG.no_point_at_end} for p in end_free_pipes]
                problems += [{'lyr': pipe_lyr, 'id': p, 'mes': LANG.pipe_input_not_complete} for p in without_diameter_pipes]
                problems += [{'lyr': point_lyr, 'id': p, 'mes': LANG.free_point} for p in free_point]
                problems += [{'lyr': point_lyr, 'id': p, 'mes': LANG.point_input_not_complete} for p in without_data_points]
                dialog.progress = 100
            DataStore.instance().problems = problems
            pythonaddins.MessageBox(LANG.system_check.format(len(problems), LANG.solve_problems if len(problems) > 0 else ''), "Warning", 0)
        except PipeSimException as e:
            pythonaddins.MessageBox(e.message, "Some thing went wrong!", 0)


class Run(object):
    """Implementation for pipesim_gis_addin.button3 (Button)"""
    def __init__(self):
        self.enabled = True
        self.checked = False

    def onClick(self):
        try:
            if is_licence_valid() is False:
                raise PipeSimException(LANG.invalid_licence)
            try:
                resp = json.loads(run_ui.exec_in_sub_process())
                method = DataStore.instance()._method_dict[resp['method']]
                software = resp['software']
                DataStore.instance().server_address = resp['server_address']
            except ValueError:
                return

            point_layer = DataStore.instance().point_layer
            pipe_layer = DataStore.instance().pipe_layer
            arcpy.SelectLayerByAttribute_management(point_layer, "CLEAR_SELECTION")
            arcpy.SelectLayerByAttribute_management(pipe_layer, "CLEAR_SELECTION")

            point_field_helper = DataStore.instance().settings['point_fields']
            pipe_field_helper = DataStore.instance().settings['pipe_fields']
            point_fields = OrderedDict([
                ("id", "OID@"),
                ("c", point_field_helper['consumption']['name']),
                ("P", point_field_helper['pressure']['name']),
                ("s", point_field_helper['is_source']['name']),
                ("a", point_field_helper['is_active_point']['name']),
                ("sp", point_field_helper['is_special']['name']),
                ("g_limit", point_field_helper['gen_limit']['name']),
            ])
            pipe_fields = OrderedDict([
                ("id", "OID@"),
                ("from", pipe_field_helper['start']['name']),
                ("to", pipe_field_helper['end']['name']),
                ("l", pipe_field_helper['length']['name']),
                ("d", pipe_field_helper['diameter']['name']),
                ("a", pipe_field_helper['is_active']['name']),
            ])

            points = arcpy.da.SearchCursor(point_layer, point_fields.values())
            pipes = arcpy.da.SearchCursor(pipe_layer, pipe_fields.values())

            req_point = [{k: v for k, v in zip(point_fields.keys(), p)} for p in points]
            req_pipe = [{k: v for k, v in zip(pipe_fields.keys(), p)} for p in pipes]
            req_settings = DataStore.instance().settings
            params = {'pipes': req_pipe, 'points': req_point, 'settings': req_settings, 'method': method}
            if software == 'network':
                try:
                    job_id = run_pipeline(params)
                    cal_ui.exec_in_server(job_id)
                    resp = get_result_pipeline(job_id)
                except Exception:
                    raise PipeSimException(LANG.run_error)
                finally:
                    terminate_job(job_id)
            else:
                try:
                    file_address, p = calculate_pipeline_in_sub_process(params)
                    resp = cal_ui.exec_in_sub_process(file_address)
                    resp = json.loads(resp)
                except Exception:
                    raise PipeSimException(LANG.run_error)
                finally:
                    command = ['TASKKILL', '/F', '/T', '/PID', str(p.pid)]
                    Popen(command, shell=True)

            if resp.get('error', None) is not None:
                raise PipeSimException(getattr(LANG, resp['error']['name']).format(resp['error']['params']))

            point_field_name = [
                'OID@',
                DataStore.instance().settings['point_fields']['pressure']['name'],
                DataStore.instance().settings['point_fields']['section']['name'],
                DataStore.instance().settings['point_fields']['generation']['name'],
            ]
            with CursorWithEditor(arcpy.da.UpdateCursor, point_layer, point_field_name) as cursor:
                for row in cursor:
                    if str(row[0]) in resp['pressures']:
                        press = float(resp['pressures'][str(row[0])])
                        row[1] = press if press > 0 else 0
                        row[2] = float(resp['sections'][str(row[0])])
                        gen = resp['gen'].get(str(row[0]))
                        row[3] = float(gen) if gen else 0
                        cursor.updateRow(row)

            pipe_field_name = [
                'OID@',
                DataStore.instance().settings['pipe_fields']['flow']['name'],
                DataStore.instance().settings['pipe_fields']['speed']['name'],
            ]
            with CursorWithEditor(arcpy.da.UpdateCursor, pipe_layer, pipe_field_name) as cursor:
                for row in cursor:
                    if str(row[0]) in resp['flows']:
                        row[1] = float(resp['flows'][str(row[0])])
                        row[2] = float(resp['speeds'][str(row[0])])
                        cursor.updateRow(row)

            pythonaddins.MessageBox(LANG.run_finish, "Info", 0)
        except PipeSimException as e:
            pythonaddins.MessageBox(e.message, "Some thing went wrong!", 0)


class Settings(object):
    """Implementation for pipesim_gis_addin.button (Button)"""
    def __init__(self):
        self.enabled = True
        self.checked = False

    def onClick(self):
        try:
            try:
                new_settings = json.loads(settings_ui.exec_in_sub_process(
                    DataStore.instance().flatten_settings(),
                    DataStore.instance().flatten_settings(DataStore.instance()._initial),
                ))
            except ValueError:
                return
            DataStore.instance().update_settings(new_settings)
            layers = [layer.name for layer in arcpy.mapping.ListLayers(DataStore.instance().current_map)]
            is_point_layer_exists = DataStore.instance().settings['point_layer'] in layers
            is_pipe_layer_exists = DataStore.instance().settings['pipe_layer'] in layers
            if is_pipe_layer_exists is False and is_point_layer_exists is False:
                ws = create_geo_database()
                force_create_layer = True
            elif is_pipe_layer_exists is False:
                ws = DataStore.instance().point_layer.workspacePath
                force_create_layer = False
            elif is_point_layer_exists is False:
                ws = DataStore.instance().pipe_layer.workspacePath
                force_create_layer = False

            if is_point_layer_exists is False:
                create_feature_layer(
                    DataStore.instance().settings['point_layer'],
                    'POINT',
                    ws,
                    force_create_layer
                )
            if is_pipe_layer_exists is False:
                create_feature_layer(
                    DataStore.instance().settings['pipe_layer'],
                    'POLYLINE',
                    ws,
                    force_create_layer
                )
            create_fields_if_not_exist(DataStore.instance().point_layer, DataStore.instance().settings['point_fields'].values())
            create_fields_if_not_exist(DataStore.instance().pipe_layer, DataStore.instance().settings['pipe_fields'].values())

        except PipeSimException as e:
            pythonaddins.MessageBox(e.message, "Some thing went wrong!", 0)


class NextProblem(object):
    """Implementation for pipesim_gis_addin.button_10 (Button)"""
    def __init__(self):
        self.enabled = True
        self.checked = False

    def onClick(self):
        df = arcpy.mapping.ListDataFrames(DataStore.instance().current_map)[0]
        point_layer = DataStore.instance().point_layer
        pipe_layer = DataStore.instance().pipe_layer
        arcpy.SelectLayerByAttribute_management(pipe_layer, "CLEAR_SELECTION")
        arcpy.SelectLayerByAttribute_management(point_layer, "CLEAR_SELECTION")
        if len(DataStore.instance().problems) == 0:
            pythonaddins.MessageBox(LANG.no_problem, "Info", 0)
            return
        problem = DataStore.instance().problems.pop(0)
        oid_name = get_layer_OID(problem['lyr'])
        arcpy.SelectLayerByAttribute_management(problem['lyr'], "NEW_SELECTION", '{} = {}'.format(oid_name, problem['id']))
        df.zoomToSelectedFeatures()
        arcpy.RefreshActiveView()
        pythonaddins.MessageBox(problem['mes'], "Error", 0)


class PointSimpleView(object):
    """Implementation for pipesim_gis_addin.button_1 (Button)"""
    def __init__(self):
        self.enabled = True
        self.checked = False

    def onClick(self):
        # map = arcpy.mapping.MapDocument("CURRENT")
        # lyr = arcpy.mapping.ListLayers(map, "point")[0]
        # sym_dict = json.loads(lyr._arc_object.getsymbology())
        lyr = DataStore.instance().point_layer
        lyr.updateLayerFromJSON(json.dumps({"drawingInfo": DataStore.instance()._point_symbols['none']}))
        arcpy.RefreshActiveView()


class PressureView(object):
    """Implementation for pipesim_gis_addin.button_2 (Button)"""
    def __init__(self):
        self.enabled = True
        self.checked = False

    def onClick(self):
        try:
            lyr_name = DataStore.instance().point_layer
            pressure_field_name = DataStore.instance().settings['point_fields']['pressure']['name']
            arcpy.SelectLayerByAttribute_management(lyr_name, "CLEAR_SELECTION")
            pressures = [row[0] for row in arcpy.da.SearchCursor(lyr_name, [pressure_field_name])]
            empty_pressures = sum(1 for p in pressures if p is None)
            if empty_pressures > 0:
                raise PipeSimException(LANG.empty_pressure)
            max_pressure = max(pressures)
            lyr = DataStore.instance().point_layer
            if max_pressure > 100:
                lyr.updateLayerFromJSON(json.dumps({"drawingInfo": DataStore.instance()._point_symbols['pressure'](
                    pressure_field_name,
                    0,
                    140,
                    145,
                    max_pressure,
                )}))
            else:
                lyr.updateLayerFromJSON(json.dumps({"drawingInfo": DataStore.instance()._point_symbols['pressure'](
                    pressure_field_name,
                    0,
                    40,
                    45,
                    max_pressure,
                )}))
            arcpy.RefreshActiveView()
        except PipeSimException as e:
            pythonaddins.MessageBox(e.message, "Some thing went wrong!", 0)


class SourceView(object):
    """Implementation for pipesim_gis_addin.button_3 (Button)"""
    def __init__(self):
        self.enabled = True
        self.checked = False

    def onClick(self):
        field_name = DataStore.instance().settings['point_fields']['is_source']['name']
        lyr = DataStore.instance().point_layer
        lyr.updateLayerFromJSON(json.dumps({"drawingInfo": DataStore.instance()._point_symbols['source'](field_name)}))
        arcpy.RefreshActiveView()


class SectionView(object):
    """Implementation for pipesim_gis_addin.button_4 (Button)"""
    def __init__(self):
        self.enabled = True
        self.checked = False

    def onClick(self):
        try:
            lyr_name = DataStore.instance().point_layer
            arcpy.SelectLayerByAttribute_management(lyr_name, "CLEAR_SELECTION")
            field_name = DataStore.instance().settings['point_fields']['section']['name']
            values = [row[0] for row in arcpy.da.SearchCursor(lyr_name, [field_name])]
            empty_values = sum(1 for p in values if p is None or p == 0)
            if empty_values > 0:
                raise PipeSimException(LANG.empty_section)
            lyr = lyr_name
            value_list = sorted(set(values))
            lyr.updateLayerFromJSON(json.dumps({"drawingInfo": DataStore.instance()._point_symbols['sections'](
                field_name,
                value_list,
            )}))
            arcpy.RefreshActiveView()
        except PipeSimException as e:
            pythonaddins.MessageBox(e.message, "Some thing went wrong!", 0)


class PipeSimpleView(object):
    """Implementation for pipesim_gis_addin.button_5 (Button)"""
    def __init__(self):
        self.enabled = True
        self.checked = False

    def onClick(self):
        lyr = DataStore.instance().pipe_layer
        lyr.updateLayerFromJSON(json.dumps({"drawingInfo": DataStore.instance()._pipe_symbols['none']}))
        arcpy.RefreshActiveView()


class SpeedView(object):
    """Implementation for pipesim_gis_addin.button_6 (Button)"""
    def __init__(self):
        self.enabled = True
        self.checked = False

    def onClick(self):
        try:
            lyr_name = DataStore.instance().pipe_layer
            field_name = DataStore.instance().settings['pipe_fields']['speed']['name']
            values = [row[0] for row in arcpy.da.SearchCursor(lyr_name, [field_name])]
            empty_values = sum(1 for p in values if p is None)
            if empty_values > 0:
                raise PipeSimException(LANG.empty_speed)
            lyr = lyr_name
            lyr.updateLayerFromJSON(json.dumps({"drawingInfo": DataStore.instance()._pipe_symbols['speed'](
                field_name,
                70,
                75,
            )}))
            arcpy.RefreshActiveView()
        except PipeSimException as e:
            pythonaddins.MessageBox(e.message, "Some thing went wrong!", 0)


class ActiveView(object):
    """Implementation for pipesim_gis_addin.button_7 (Button)"""
    def __init__(self):
        self.enabled = True
        self.checked = False

    def onClick(self):
        field_name = DataStore.instance().settings['pipe_fields']['is_active']['name']
        lyr = DataStore.instance().pipe_layer
        lyr.updateLayerFromJSON(json.dumps({"drawingInfo": DataStore.instance()._pipe_symbols['active'](field_name)}))
        arcpy.RefreshActiveView()


class DiameterView(object):
    """Implementation for pipesim_gis_addin.button_7 (Button)"""
    def __init__(self):
        self.enabled = True
        self.checked = False

    def onClick(self):
        field_name = DataStore.instance().settings['pipe_fields']['diameter']['name']
        lyr = DataStore.instance().pipe_layer
        lyr.updateLayerFromJSON(json.dumps({"drawingInfo": DataStore.instance()._pipe_symbols['diameter'](field_name)}))
        arcpy.RefreshActiveView()


class CreateReport(object):
    """Implementation for pipesim_gis_addin.button_11 (Button)"""
    def __init__(self):
        self.enabled = True
        self.checked = False

    def onClick(self):
        try:
            point_layer = DataStore.instance().point_layer
            pipe_layer = DataStore.instance().pipe_layer
            arcpy.SelectLayerByAttribute_management(point_layer, "CLEAR_SELECTION")
            arcpy.SelectLayerByAttribute_management(pipe_layer, "CLEAR_SELECTION")

            point_field_helper = DataStore.instance().settings['point_fields']
            pipe_field_helper = DataStore.instance().settings['pipe_fields']
            point_fields = [
                "OID@",
                point_field_helper['consumption']['name'],
                point_field_helper['pressure']['name'],
            ]
            pipe_fields = [
                pipe_field_helper['flow']['name'],
                pipe_field_helper['end']['name'],
                pipe_field_helper['start']['name'],
                pipe_field_helper['diameter']['name'],
                pipe_field_helper['speed']['name'],
            ]

            points = arcpy.da.SearchCursor(point_layer, point_fields)
            pipes = arcpy.da.SearchCursor(pipe_layer, pipe_fields)
            points_dict = {p[0]: {'c': p[1], 'p': p[2], 'pipes': []} for p in points}
            nps_dict = OrderedDict(DataStore.instance().settings['nps_dict'])
            for p in pipes:
                p_drop = abs(points_dict[p[1]]['p'] - points_dict[p[2]]['p'])
                dia = nps_dict[str(int(p[3]))] if DataStore.instance().settings['use_nps'] else "{0:.4f}".format(p[3])
                if p[4] >= 0:
                    points_dict[p[1]]['pipes'].append({'c': p[0], 'from': p[2], 'p_drop': p_drop, 'dia': dia, 'speed': p[4]})
                    points_dict[p[2]]['pipes'].append({'c': p[0], 'to': p[1], 'p_drop': p_drop, 'dia': dia, 'speed': p[4]})
                else:
                    points_dict[p[1]]['pipes'].append({'c': -p[0], 'to': p[2], 'p_drop': p_drop, 'dia': dia, 'speed': -p[4]})
                    points_dict[p[2]]['pipes'].append({'c': -p[0], 'from': p[1], 'p_drop': p_drop, 'dia': dia, 'speed': -p[4]})
            data = []
            for point_id, point in points_dict.iteritems():
                data.append([str(point_id), "{0:.2f}".format(point['p']), "{0:.2f}".format(point['c']), '', '', '', '', ''])
                for p in point['pipes']:
                    data.append(['', '', "{0:.2f}".format(p['c']), str(p.get('to', '')), str(p.get('from', '')),
                                 "{0:.4f}".format(p['p_drop']), p['dia'], "{0:.2f}".format(p['speed'])])
            selected_format = pipesim_format_select_ui.exec_in_sub_process(["xls", "pdf"])
            if len(selected_format) == 0:
                raise PipeSimException(LANG.no_format)
            save_address, _ = path.split(pythonaddins.SaveDialog('save location', 'result'))
            style_address = path.dirname(__file__) + '\\report\\result.rlf'
            report_fields = ['node_id', 'pressure', 'load', 'to', 'from_', 'press_drop', 'inside_dia', 'velocity']
            for f in selected_format:
                output_address = save_address + '\\result.' + f
                export_rlf(output_address, data, report_fields, style_address)
        except PipeSimException as e:
            pythonaddins.MessageBox(e.message, "Some thing went wrong!", 0)
