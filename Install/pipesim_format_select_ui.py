import json
import ttk
import sys
from Tkinter import Tk, IntVar
from os import path
from subprocess import Popen, PIPE


class Application(ttk.Frame):
    def createWidgets(self):
        # formula widget
        formula = ttk.Labelframe(self)
        formula.configure(height='200', text='select format', width='200')
        # igt widget
        for i, opt in enumerate(self.options):
            label = ttk.Checkbutton(formula)
            label.configure(text=opt, variable=self.binding[i], onvalue=1, offvalue=0)
            label.grid(column='0', row=i, sticky='w')
        formula.grid(column='0', row='0')
        formula.columnconfigure(0, minsize='150')
        # calculate widget
        calculate = ttk.Button(self, command=self.execute)
        calculate.configure(text='ok')
        calculate.grid(column='0', row='1')
        self.grid(column='0', row='0')
        self.rowconfigure(1, minsize='40')
        self.columnconfigure(0, pad='5')

    def __init__(self, master=None, data=[]):
        ttk.Frame.__init__(self, master)
        self.configure(height='200', padding='20', width='200')
        self.binding = [IntVar() for i in data]
        self.options = data
        self.createWidgets()

    def execute(self):
        res = [self.options[i] for i, item in enumerate(self.binding) if item.get() == 1]
        print(json.dumps(res))
        self.quit()


def main():
    options = json.loads(sys.argv[1] if len(sys.argv) > 1 else '[]')
    if len(options) <= 1:
        print(json.dumps(options))
    else:
        root = Tk()
        root.title('select format')
        app = Application(master=root, data=options)
        app.mainloop()
        try:
            root.destroy()
        except:
            pass


def exec_in_sub_process(options):
    file_path = path.abspath(__file__)
    data = json.dumps(options)
    proc = Popen([file_path, data], shell=True, stdout=PIPE, stderr=PIPE, bufsize=1)
    stdoutdata, stderrdata = proc.communicate()
    proc.terminate()
    try:
        res = json.loads(stdoutdata.strip())
    except Exception:
        res = []
    return res


if __name__ == '__main__':
    main()
