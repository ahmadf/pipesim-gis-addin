# -*- coding: utf-8 -*-
import arcpy
import pythonaddins
import json
import os
import numpy as np
import hashlib
from copy import deepcopy
from collections import OrderedDict
from os import path
from translation import Language
LANG = Language()


class PipeSimException(Exception):
    pass


class DataStore(object):
    _instance = None
    problems = []
    server_address = 'localhost:8000'
    _initial = {
        'constants': {
            'cf': 0.7,
            'E': 1,
            't0': 520,
            'p0': 14.7,
            't': 520,
            'ro': 0.57,
            'vis': 0.000008,
        },
        'units': {
            'flow_unit': 'SCMH',
            'speed_unit': 'ft/s',
            'length_unit': 'm',
            'pressure_unit': 'psig',
            'temperature_unit': 'R',
            'diameter_unit': 'In',
        },
        'use_nps': True,
        'nps_dict': [
            [2, 2.087],
            [4, 4.156],
            [6, 6.281],
            [8, 8.249],
            [10, 10.312],
            [12, 12.25],
            [16, 15.438],
            [20, 19.432],
            [24, 23.25],
            [63, 2.02],
            [90, 2.9],
            [110, 3.54],
            [125, 4.026],
            [160, 5.54],
            [200, 6.44],
            [225, 7.2],
        ],
        'point_layer': 'point',
        'point_fields': OrderedDict([
            ('consumption', {'name': 'phf', 'db_attrs': {'field_type': 'FLOAT', 'field_is_nullable': True}}),
            ('pressure', {'name': 'p', 'db_attrs': {'field_type': 'FLOAT', 'field_is_nullable': True}}),
            ('is_source', {'name': 'p0', 'db_attrs': {'field_type': 'SHORT', 'field_is_nullable': True}}),
            ('section', {'name': 'sec', 'db_attrs': {'field_type': 'SHORT', 'field_is_nullable': True}}),
            ('is_active_point', {'name': 'is_act_p', 'db_attrs': {'field_type': 'SHORT', 'field_is_nullable': True}, 'default': 1}),
            ('is_special', {'name': 'is_s', 'db_attrs': {'field_type': 'SHORT', 'field_is_nullable': True}, 'default': 0}),
            ('gen_limit', {'name': 'gen_limit', 'db_attrs': {'field_type': 'FLOAT', 'field_is_nullable': True}, 'default': 0}),
            ('generation', {'name': 'generation', 'db_attrs': {'field_type': 'FLOAT', 'field_is_nullable': True}, 'default': 0}),
            ('station_name', {'name': 'station_name', 'db_attrs': {'field_type': 'TEXT', 'field_is_nullable': True}}),
        ]),
        'pipe_layer': 'pipe',
        'pipe_fields': OrderedDict([
            ('flow', {'name': 'flow', 'db_attrs': {'field_type': 'FLOAT', 'field_is_nullable': True}}),
            ('speed', {'name': 'speed', 'db_attrs': {'field_type': 'FLOAT', 'field_is_nullable': True}}),
            ('start', {'name': 'start', 'db_attrs': {'field_type': 'SHORT', 'field_is_nullable': True}}),
            ('end', {'name': 'end', 'db_attrs': {'field_type': 'SHORT', 'field_is_nullable': True}}),
            ('length', {'name': 'length', 'db_attrs': {'field_type': 'FLOAT', 'field_is_nullable': True}}),
            ('diameter', {'name': 'qotr', 'db_attrs': {'field_type': 'FLOAT', 'field_is_nullable': True}}),
            ('is_active', {'name': 'is_active', 'db_attrs': {'field_type': 'SHORT', 'field_is_nullable': True}, 'default': 1}),
        ]),
    }
    _method_dict = {
        'igt': 'IGT',
        'pen_A': 'PenhandleA',
        'pen_B': 'PenhandleB',
        'wey': 'Weymouth',
        'sgh': 'SpiltzGlass',
    }
    _colors = [
        [255, 0, 0, 255],
        [128, 0, 0, 255],
        [255, 255, 0, 255],
        [128, 128, 0, 255],
        [0, 255, 0, 255],
        [0, 128, 0, 255],
        [0, 255, 255, 255],
        [0, 128, 128, 255],
        [0, 0, 255, 255],
        [0, 0, 128, 255],
        [255, 0, 255, 255],
        [128, 0, 128, 255],
    ]
    diameter_colors = {
        '10': [197, 121, 255, 255],
        '110': [165, 124, 84, 255],
        '12': [127, 31, 0, 255],
        '125': [0, 255, 0, 255],
        '16': [76, 57, 38, 255],
        '160': [0, 0, 255, 255],
        '2': [255, 0, 0, 255],
        '20': [105, 15, 10, 255],
        '200': [0, 95, 127, 255],
        '225': [127, 0, 63, 255],
        '24': [165, 0, 41, 255],
        '30': [124, 0, 165, 255],
        '36': [19, 28, 38, 255],
        '4': [165, 124, 82, 255],
        '6': [0, 0, 255, 255],
        '63': [255, 63, 0, 255],
        '8': [90, 63, 65, 255],
        '90': [255, 191, 127, 255],
    }
    _pipe_symbols = {
        'none': {u'renderer': {u'symbol': {u'color': [0, 0, 0, 255], u'width': 1.0, u'style': u'esriSLSSolid', u'type': u'esriSLS'}, u'type': u'simple'}, u'transparency': 0},
        'speed': lambda field_name, warning_limit, error_limit: {u'renderer': {u'field': field_name, u'classBreakInfos': [
            {u'classMaxValue': -error_limit, u'classMinValue': -10000, u'symbol': {u'color': [239, 19, 41, 255], u'width': 0.5, u'style': u'esriSLSSolid', u'type': u'esriSLS'}, u'label': u'سرعت زیاد(>{})'.format(error_limit)},
            {u'classMaxValue': -warning_limit, u'classMinValue': -error_limit, u'symbol': {u'color': [255, 242, 0, 255], u'width': 1, u'style': u'esriSLSSolid', u'type': u'esriSLS'}, u'label': u'سرعت بحرانی({} - {})'.format(warning_limit, error_limit)},
            {u'classMaxValue': -0.05, u'classMinValue': -warning_limit, u'symbol': {u'color': [0, 255, 76, 255], u'width': 0.5, u'style': u'esriSLSSolid', u'type': u'esriSLS'}, u'label': u'سرعت مناسب(<{})'.format(warning_limit)},
            {u'classMaxValue': 0.05, u'classMinValue': -0.05, u'symbol': {u'color': [0, 255, 76, 255], u'width': 0.5, u'style': u'esriSLSSolid', u'type': u'esriSLS'}, u'label': u'سرعت مناسب بدون جهت'},
            {u'classMaxValue': warning_limit, u'classMinValue': 0.05, u'symbol': {u'color': [0, 255, 76, 255], u'width': 0.5, u'style': u'esriSLSSolid', u'type': u'esriSLS'}, u'label': u'سرعت مناسب(<{})'.format(warning_limit)},
            {u'classMaxValue': error_limit, u'classMinValue': warning_limit, u'symbol': {u'color': [255, 242, 0, 255], u'width': 0.5, u'style': u'esriSLSSolid', u'type': u'esriSLS'}, u'label': u'سرعت بحرانی({} - {})'.format(warning_limit, error_limit)},
            {u'classMaxValue': 10000, u'classMinValue': error_limit, u'symbol': {u'color': [239, 19, 41, 255], u'width': 0.5, u'style': u'esriSLSSolid', u'type': u'esriSLS'}, u'label': u'سرعت زیاد(>{})'.format(error_limit)}
        ], u'type': u'classBreaks', u'defaultSymbol': {u'color': [255, 0, 0, 255], u'width': 0.5, u'style': u'esriSLSSolid', u'type': u'esriSLS'}, u'minValue': -10000}, u'transparency': 0},
        'active': lambda field_name: {u'renderer': {u'uniqueValueInfos': [
            {u'symbol': {u'color': [0, 0, 0, 255], u'width': 1, u'style': u'esriSLSDash', u'type': u'esriSLS'}, u'value': u'0', u'label': u'غیر فعال'},
            {u'symbol': {u'color': [0, 0, 0, 255], u'width': 1, u'style': u'esriSLSSolid', u'type': u'esriSLS'}, u'value': u'1', u'label': u'فعال'}
        ], u'field1': field_name, u'type': u'uniqueValue', u'defaultLabel': u'<all other values>', u'defaultSymbol': {u'color': [97, 0, 158, 255], u'width': 1.0, u'style': u'esriSLSSolid', u'type': u'esriSLS'}}, u'transparency': 0},
        'diameter': lambda field_name: {u'renderer': {u'uniqueValueInfos': [
            {u'symbol': {u'color': j, u'style': u'esriSLSSolid', u'type': u'esriSLS', u'width': 1.2}, u'value': i, u'label': i}
        for i,j in DataStore.diameter_colors.items()], u'field1': field_name, u'type': u'uniqueValue', u'defaultLabel': u'<all other values>', u'defaultSymbol': {u'color': [97, 0, 158, 255], u'width': 1.2, u'style': u'esriSLSSolid', u'type': u'esriSLS'}}, u'transparency': 0},
    }
    _point_symbols = {
        'none': {u'renderer': {u'symbol': {u'color': [0, 166, 116, 255], u'style': u'esriSMSCircle', u'type': u'esriSMS', u'outline': {u'color': [0, 0, 0, 255], u'width': 1.0}, u'size': 4.0}, u'type': u'simple'}, u'transparency': 0},
        'pressure': lambda field_name, min_val, mid1_val, mid2_val, max_val: {u'renderer': {u'field': field_name, u'classBreakInfos': [
            {u'classMaxValue': mid1_val, u'classMinValue': min_val, u'symbol': {u'color': [239, 19, 41, 255], u'style': u'esriSMSCircle', u'type': u'esriSMS', u'outline': {u'color': [0, 0, 0, 255], u'width': 1.0}, u'size': 4.0}, u'label': u'فشار پایین({} - {})'.format(min_val, mid1_val)},
            {u'classMaxValue': mid2_val, u'classMinValue': mid1_val, u'symbol': {u'color': [255, 242, 0, 255], u'style': u'esriSMSCircle', u'type': u'esriSMS', u'outline': {u'color': [0, 0, 0, 255], u'width': 1.0}, u'size': 4.0}, u'label': u'فشار بحرانی({} - {})'.format(mid1_val, mid2_val)},
            {u'classMaxValue': max_val, u'classMinValue': mid2_val, u'symbol': {u'color': [0, 255, 76, 255], u'style': u'esriSMSCircle', u'type': u'esriSMS', u'outline': {u'color': [0, 0, 0, 255], u'width': 1.0}, u'size': 4.0}, u'label': u'فشار مناسب({} - {})'.format(mid2_val, max_val)}
        ], u'type': u'classBreaks', u'defaultSymbol': {u'color': [255, 255, 128, 255], u'style': u'esriSMSCircle', u'type': u'esriSMS', u'outline': {u'color': [0, 0, 0, 255], u'width': 1.0}, u'size': 4.0}, u'minValue': min_val}, u'transparency': 0},
        'source': lambda field_name: {u'renderer': {u'uniqueValueInfos': [
            {u'symbol': {u'color': [0, 0, 0, 255], u'style': u'esriSMSCircle', u'type': u'esriSMS', u'size': 6.0}, u'value': u'0', u'label': u'مصرف کننده'},
            {u'symbol': {u'color': [255, 0, 0, 255], u'style': u'esriSMSCircle', u'type': u'esriSMS', u'size': 12.0}, u'value': u'1', u'label': u'ورودی'}
        ], u'field1': field_name, u'type': u'uniqueValue', u'defaultLabel': u'<all other values>', u'defaultSymbol': {u'color': [168, 90, 0, 255], u'style': u'esriSMSCircle', u'type': u'esriSMS', u'outline': {u'color': [0, 0, 0, 255], u'width': 1.0}, u'size': 4.0}}, u'transparency': 0},
        'sections': lambda field_name, value_list: {u'renderer': {u'uniqueValueInfos': [
            {u'symbol': {u'color': DataStore._colors[i % 12], u'style': u'esriSMSCircle', u'type': u'esriSMS', u'outline': {u'color': [0, 0, 0, 255], u'width': 1.0}, u'size': 4.0}, u'value': v, u'label': u'ناحیه {}'.format(v)} for i, v in enumerate(value_list)
        ], u'field1': field_name, u'type': u'uniqueValue', u'defaultLabel': u'<all other values>', u'defaultSymbol': {u'color': [133, 0, 11, 255], u'style': u'esriSMSCircle', u'type': u'esriSMS', u'outline': {u'color': [0, 0, 0, 255], u'width': 1.0}, u'size': 4.0}}, u'transparency': 0},
    }

    def __init__(self):
        self.current_map = arcpy.mapping.MapDocument("CURRENT")
        if not self.current_map.filePath:
            raise PipeSimException(LANG.mxd_not_saved)
        dir_name, file_name = path.split(self.current_map.filePath)
        try:
            self.update_settings(self.flatten_settings(json.load(open(dir_name + '\\pipesim_settings.json'))))
        except Exception:
            self.settings = deepcopy(self._initial)

    @classmethod
    def instance(self):
        if self._instance is None:
            self._instance = DataStore()
        return self._instance

    def flatten_settings(self, settings=None):
        if settings is None:
            settings = self.settings
        result = {}
        result.update(settings['constants'])
        result.update(settings['units'])
        result['point_layer_name'] = settings['point_layer']
        result.update({k: v['name'] for k, v in settings['point_fields'].iteritems()})
        result['pipe_layer_name'] = settings['pipe_layer']
        result.update({k: v['name'] for k, v in settings['pipe_fields'].iteritems()})
        result['nps'] = settings['nps_dict'] if settings['use_nps'] else []
        return result

    def update_settings(self, user_input):
        new_settings = deepcopy(self._initial)
        for i in new_settings['constants']:
            new_settings['constants'][i] = user_input[i]
        for i in new_settings['units']:
            new_settings['units'][i] = user_input[i]
        new_settings['point_layer'] = user_input['point_layer_name']
        for k, v in new_settings['point_fields'].iteritems():
            v['name'] = user_input[k]
        new_settings['pipe_layer'] = user_input['pipe_layer_name']
        for k, v in new_settings['pipe_fields'].iteritems():
            v['name'] = user_input[k]
        if len(user_input['nps']) > 0:
            new_settings['use_nps'] = True
            new_settings['nps_dict'] = user_input['nps']
        else:
            new_settings['use_nps'] = False

        self.settings = new_settings
        dir_name, file_name = path.split(self.current_map.filePath)
        with open(dir_name + '\\pipesim_settings.json', 'w') as outfile:
            try:
                json.dump(self.settings, outfile)
            except Exception:
                raise PipeSimException(LANG.not_save_error)

    @property
    def file_address(self):
        dir_name, file_name = path.split(self.current_map.filePath)
        return dir_name + '\\out.txt'

    @property
    def point_layer(self):
        for layer in arcpy.mapping.ListLayers(self.current_map):
            if layer.name == self.settings['point_layer']:
                return layer
        raise PipeSimException(LANG.layer_name_error.format(LANG.points_table))

    @property
    def pipe_layer(self):
        for layer in arcpy.mapping.ListLayers(self.current_map):
            if layer.name == self.settings['pipe_layer']:
                return layer
        raise PipeSimException(LANG.layer_name_error.format(LANG.pipes_table))

    def select_shape_by_location(self, lyr, x, y, is_exact=False):
        mxd = self.current_map
        pointGeom = arcpy.PointGeometry(arcpy.Point(x, y), mxd.activeDataFrame.spatialReference)
        searchdistance = mxd.activeDataFrame.scale * 3 / 96.0
        if is_exact:
            arcpy.management.SelectLayerByLocation(lyr, "INTERSECT", pointGeom)
        else:
            arcpy.management.SelectLayerByLocation(lyr, "INTERSECT", pointGeom, "%d INCHES" % searchdistance)


def create_fields_if_not_exist(table, fields):
    table_fields = [f.name for f in arcpy.ListFields(table)]
    for f in fields:
        if f['name'] not in table_fields:
            arcpy.management.AddField(table, f['name'], **f['db_attrs'])
            if f.get('default') is not None:
                with CursorWithEditor(arcpy.da.UpdateCursor, table, f['name']) as cursor:
                    for row in cursor:
                        row[0] = f.get('default')
                        cursor.updateRow(row)


def get_layer_OID(lyr):
    oid_fieldname = arcpy.ListFields(lyr,"","OID")
    if len(oid_fieldname) != 1:
        raise Exception("oid len is not zero")
    return oid_fieldname[0].name


def create_geo_database():
    res = pythonaddins.MessageBox(LANG.db_not_found, 'Warning', 1)
    if res == 'OK':
        save_address, name = path.split(pythonaddins.SaveDialog('save location', 'db'))
        if save_address is not None:
            if path.splitext(name)[1] != '.gdb':
                name += '.gdb'
            arcpy.CreateFileGDB_management(save_address, name)
            geo_address = path.join(save_address, name)
            sp = arcpy.SpatialReference(32640)
            arcpy.CreateFeatureDataset_management(geo_address, 'pipesim', sp)
            return path.join(geo_address, 'pipesim')
    raise PipeSimException(LANG.db_not_found_exception)


def create_feature_layer(layer_name, layer_type, save_address, is_force):
    res = None
    df = arcpy.mapping.ListDataFrames(DataStore.instance().current_map)[0]
    sp = df.spatialReference
    if is_force is False:
        res = pythonaddins.MessageBox(LANG.layer_not_found.format(layer_name, path.join(save_address, layer_name)), 'Warning', 1)
        if res != 'OK':
            raise PipeSimException(LANG.layer_not_found_exception.format(layer_name))
    if sp.PCSCode == 0:
        res = pythonaddins.MessageBox(LANG.spatial_refrence_not_found.format("WGS_1984_UTM_Zone_40N"), 'Warning', 1)
        if res != 'OK':
            raise PipeSimException(LANG.spatial_refrence_not_found_exception)
        sp = arcpy.SpatialReference(32640)
    arcpy.CreateFeatureclass_management(save_address, layer_name, layer_type, "", "DISABLED", "DISABLED", sp)


def result_set_should_have_one(cursor):
    rows = [row for row in cursor]
    if len(rows) == 0:
        raise IndexError
    if len(rows) > 1:
        raise PipeSimException(LANG.multiple_points.format(', '.join([str(r[0]) for r in rows])))
    return rows[0]


def update_connection(pipe):
    from_point, to_point = pipe.firstPoint, pipe.lastPoint
    lyr = DataStore.instance().point_layer
    DataStore.instance().select_shape_by_location(lyr, from_point.X, from_point.Y, True)
    with arcpy.da.SearchCursor(lyr, ['OID@']) as cursor:
        from_point_id = result_set_should_have_one(cursor)[0]

    DataStore.instance().select_shape_by_location(lyr, to_point.X, to_point.Y, True)
    with arcpy.da.SearchCursor(lyr, ['OID@']) as cursor:
        to_point_id = result_set_should_have_one(cursor)[0]
    return from_point_id, to_point_id


def find_end_points(pipe, points):
    from_point = np.array((pipe.firstPoint.X, pipe.firstPoint.Y))
    to_point = np.array((pipe.lastPoint.X, pipe.lastPoint.Y))
    tolerance = min(0.05 * np.linalg.norm(from_point - to_point), 0.3048)
    lyr = DataStore.instance().point_layer
    from_point_id, to_point_id = -1, -1
    oid_name = get_layer_OID(lyr)
    for point_id in points:
        point_shape = next(arcpy.da.SearchCursor(lyr, ['OID@', 'SHAPE@'], '{} = {}'.format(oid_name, point_id)))[1]
        point_location = np.array((point_shape.firstPoint.X, point_shape.firstPoint.Y))
        dis_to_start = np.linalg.norm(point_location - from_point)
        dis_to_end = np.linalg.norm(point_location - to_point)
        if dis_to_start < tolerance:
            from_point_id = point_id
        elif dis_to_end < tolerance:
            to_point_id = point_id
    return from_point_id, to_point_id


def get_connected_pipes(point_id):
    settings = DataStore.instance().settings
    lyr = DataStore.instance().pipe_layer
    result = []
    columns = ['OID@', 'SHAPE@'] + [j['name'] for _, j in settings['pipe_fields'].items()]
    pipe1 = [i for i in arcpy.da.SearchCursor(lyr, columns, '{} = {}'.format(settings['pipe_fields']['start']['name'], point_id))]
    pipe2 = [i for i in arcpy.da.SearchCursor(lyr, columns, '{} = {}'.format(settings['pipe_fields']['end']['name'], point_id))]
    return pipe1 + pipe2

def get_unique():
    import _winreg
    registry = _winreg.HKEY_LOCAL_MACHINE
    address = 'SOFTWARE\\Microsoft\\Cryptography'
    keyargs = _winreg.KEY_READ | _winreg.KEY_WOW64_64KEY
    key = _winreg.OpenKey(registry, address, 0, keyargs)
    value = _winreg.QueryValueEx(key, 'MachineGuid')
    _winreg.CloseKey(key)
    return hashlib.sha224(value[0]).hexdigest()


def export_rlf(output_address, data, fields, template):
    report_table = None
    try:
        report_table = "report_temp"
        arcpy.CreateTable_management("in_memory", report_table)
        report_fields = [{'name': field, 'db_attrs': {'field_type': 'TEXT', 'field_is_nullable': True}} for field in fields]
        create_fields_if_not_exist(report_table, report_fields)
        with arcpy.da.InsertCursor(report_table, [f['name'] for f in report_fields]) as cursor:
            for row in data:
                cursor.insertRow(row[:len(fields)])

        report_lyr = arcpy.mapping.ListTableViews(DataStore.instance().current_map, report_table)[0]
        if os.path.isfile(output_address):
            try:
                os.remove(output_address)
            except Exception:
                raise PipeSimException(LANG.file_in_use_error)
        title = "Result"
        arcpy.mapping.ExportReport(report_lyr, template, output_address, "ALL", title)
    finally:
        if report_table is not None:
            arcpy.Delete_management(report_table)
            arcpy.Delete_management("in_memory/" + report_table)


class CursorWithEditor(object):
    """wrapper class for arcpy.da.*Cursor, to automatically
    implement editing (required for versioned data, and data with
    geometric networks, topologies, network datasets, and relationship
    classes"""
    def __init__(self, func, *args, **kwargs):
        """initiate wrapper class for update cursor.  Supported args:

        in_table, field_names, where_clause=None, spatial_reference=None,
        explode_to_points=False, sql_clause=(None, None)
        """
        self.args = args
        self.kwargs = kwargs
        self.edit = None
        self.func = func

    def __enter__(self):
        ws = None
        if self.args:
            ws = self.args[0].workspacePath
        elif 'in_table' in self.kwargs:
            ws = self.kwargs['in_table'].workspacePath
        self.edit = arcpy.da.Editor(ws)
        self.edit.startEditing()
        self.edit.startOperation()
        return self.func(*self.args, **self.kwargs)

    def __exit__(self, type, value, traceback):
        self.edit.stopOperation()
        self.edit.stopEditing(True)
        self.edit = None
