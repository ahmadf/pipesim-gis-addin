import json
import ttk
import sys
import Tkinter as tk
from Tkinter import Tk, StringVar, IntVar
from os import path
from subprocess import Popen, PIPE
from translation import Language
from helper import DataStore
from pipe_steady_client import check_pipeline
LANG = Language()


class Application(ttk.Frame):
    def createWidgets(self):
        # solver_progress widget
        solver_progress = ttk.Progressbar(self)
        solver_progress.configure(length='250', orient='horizontal', variable=self.binding['progress'])
        solver_progress.grid(column='0', row='1')
        # detail_label widget
        detail_label = ttk.Label(self)
        detail_label.configure(text='Detail:')
        detail_label.grid(column='0', row='2', sticky='w')
        # frame_label widget
        frame_label = ttk.Label(self)
        frame_label.configure(textvariable=self.binding['label'])
        frame_label.grid(column='0', row='0')
        # done_btn widget
        self.done_btn = ttk.Button(self)
        self.done_btn.configure(state='disabled', text='done', command=self.export_result)
        self.done_btn.grid(column='0', row='4')
        # Frame_1 widget
        Frame_1 = tk.Frame(self)
        Frame_1.configure(height='200', width='200')
        # detail_value widget
        self.detail = tk.Text(Frame_1)
        self.detail_scroll = tk.Scrollbar(Frame_1)
        self.detail_scroll.configure(orient='vertical', relief='ridge', command=self.detail.yview)
        self.detail_scroll.grid(column='1', ipady='25', row='0')
        self.detail.configure(height='6', width='50', yscrollcommand=self.detail_scroll.set)
        self.detail.grid(column='0', row='0')
        # detail_scroll widget
        Frame_1.grid(column='0', row='3')
        Frame_1.rowconfigure(0, minsize='0')
        self.grid(column='0', row='0')
        self.rowconfigure(0, minsize='50')

    def __init__(self, master=None, file_address=None, job_id=None, server_address=None):
        ttk.Frame.__init__(self, master)
        self.configure(height='200', padding='20', width='200')
        self.job_id = job_id
        self.server_address = server_address
        self.counter = 0
        self.fp = open(file_address, 'r') if file_address is not None else None

        self.binding = {
            'progress': IntVar(),
            'label': StringVar(),
        }
        self.binding['label'].set('Waiting ...')
        self.createWidgets()

        master.after(500, self.read_status)

    def read_status(self):
        if self.fp is not None:
            self.read_file()
        else:
            self.read_server()

    def read_server(self):
        res = check_pipeline(self.job_id, self.server_address)
        lines = res['lines']
        for res in lines[self.counter:]:
            if res and 'label' in res:
                self.update_widget(json.loads(res))
            else:
                break
        self.counter = len(lines)
        if 'done' in res:
            self.done_btn['state'] = 'normal'
        elif 'error' in res:
            self.binding['progress'].set(0)
            self.binding['label'].set('an error occured')
            self.done_btn['state'] = 'normal'
        else:
            self.master.after(1000, self.read_server)

    def read_file(self):
        try:
            while True:
                res = self.fp.readline().strip()
                if res and 'label' in res:
                    self.update_widget(json.loads(res))
                else:
                    break
        except Exception as e:
            print(e)
        if 'pressures' in res:
            self.done_btn['state'] = 'normal'
        elif 'error' in res:
            print(res)
            self.binding['progress'].set(0)
            self.binding['label'].set('an error occured')
            self.done_btn['state'] = 'normal'
        else:
            self.master.after(500, self.read_file)

    def update_widget(self, data):
        self.binding['progress'].set(data['progress'])
        self.binding['label'].set(data['label'])
        if data['mes']:
            self.detail.insert(tk.END, '{}\n'.format(data['mes']))
            self.detail.see("end")

    def export_result(self):
        if self.binding['progress'].get() == 100 and self.fp is not None:
            self.fp.seek(0)
            lines = self.fp.read().splitlines()
            last_line = lines[-1]
            print(last_line)
        self.quit()


def main():
    root = Tk()
    root.title(LANG.run_ui)
    data = json.loads(sys.argv[1])
    app = Application(master=root, file_address=data.get('file', None), job_id=data.get('job_id', None), server_address=data.get('server_address', None))
    app.mainloop()
    try:
        root.destroy()
    except:
        pass


def exec_in_sub_process(file_address):
    file_path = path.abspath(__file__)
    data = json.dumps({'file': file_address})
    proc = Popen([file_path, data], shell=True, stdout=PIPE, stderr=PIPE, bufsize=1)
    stdoutdata, stderrdata = proc.communicate()
    print(stderrdata)
    proc.terminate()
    return stdoutdata.strip()


def exec_in_server(job_id):
    file_path = path.abspath(__file__)
    data = json.dumps({'job_id': job_id, 'server_address': DataStore.instance().server_address})
    proc = Popen([file_path, data], shell=True, stdout=PIPE, stderr=PIPE, bufsize=1)
    stdoutdata, stderrdata = proc.communicate()
    proc.terminate()
    return stdoutdata.strip()


if __name__ == '__main__':
    main()
