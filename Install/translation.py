# -*- coding: utf-8 -*-
class Language(object):
    check_ui = {'en': 'check network', 'fa': u'بررسی خط لوله'}
    check_all_pipes = {'en': 'change all pipe connections', 'fa': u'تغییر تمام لوله‌ها'}
    check_new_pipes = {'en': 'only check new pipes connection', 'fa': u'تغییر لوله‌هایی که جدیدا اضافه شده است'}
    check = {'en': 'check', 'fa': u'بررسی'}

    pipe_ui = {'en': 'pipe information', 'fa': u'مشخصات لوله'}
    pipe_id = {'en': 'pipe id', 'fa': u'شماره لوله'}
    from_node = {'en': 'from node', 'fa': u'از گره'}
    to_node = {'en': 'to node', 'fa': u'به گره'}
    length = {'en': 'length', 'fa': u'طول'}
    diameter = {'en': 'diameter', 'fa': u'قطر'}
    is_active = {'en': 'is active', 'fa': u'فعال بودن'}
    speed = {'en': 'speed', 'fa': u'سرعت'}
    flow = {'en': 'flow', 'fa': u'دبی حجمی'}
    save = {'en': 'save', 'fa': u'ذخیره'}
    edit_connection = {'en': 'edit connection', 'fa': u'تغییر اتصالات'}

    point_ui = {'en': 'point information', 'fa': u'مشخصات گره'}
    node_id = {'en': 'node id', 'fa': u'شماره گره'}
    section = {'en': 'section id', 'fa': u'شماره ناحیه'}
    is_source = {'en': 'type', 'fa': u'نوع'}
    generation_limit = {'en': 'generation limit', 'fa': u'محدودیت تولید'}
    station_name = {'en': 'station name', 'fa': u'نام ایستگاه'}
    generation = {'en': 'generation', 'fa': u'تولید'}
    pressure = {'en': 'pressure', 'fa': u'فشار'}
    consumption = {'en': 'consumption', 'fa': u'مصرف'}
    is_special = {'en': 'special user', 'fa': u'مشترک ویژه'}

    run_ui = {'en': 'calculation options', 'fa': u'محاسبات هیدرولیک'}
    select_formula = {'en': 'select formula', 'fa': u'انتخاب فرمول'}
    software_type = {'en': 'software type', 'fa': u'انتخاب نرم افزار'}
    net_label = {'en': 'server address', 'fa': u'آدرس سرور'}
    calculate = {'en': 'calculate', 'fa': u'محاسبه'}

    settings_ui = {'en': 'settings', 'fa': u'تنظیمات'}
    constants = {'en': 'equation constants', 'fa': u'ثوابت معادلات'}
    cf = {'en': 'cf', 'fa': u'ضریب همزمانی(cf)'}
    E = {'en': 'E', 'fa': u'راندمان(E)'}
    t0 = {'en': 't0', 'fa': u'دمای مبنا(t0)'}
    p0 = {'en': 'p0', 'fa': u'فشار مبنا(p0)'}
    t = {'en': 't', 'fa': u'دمای گاز(t)'}
    ro = {'en': 'ro', 'fa': u'چگالی ویژه(ro)'}
    vis = {'en': 'vis', 'fa': u'ویسکوزیته(vis)'}
    pipe_settings = {'en': 'pipe settings', 'fa': u'تنظیمات لوله‌ها'}
    nps_number = {'en': 'pipe size count', 'fa': u'تعداد سایز لوله'}
    unit_settings = {'en': 'unit settings', 'fa': u'تنظیمات واحدها'}
    flow_unit = {'en': 'flow units', 'fa': u'واحد دبی حجمی'}
    length_unit = {'en': 'length units', 'fa': u'واحد طول'}
    pressure_unit = {'en': 'pressure units', 'fa': u'واحد فشار'}
    temp_unit = {'en': 'temperature units', 'fa': u'واحد دما'}
    temp_unit = {'en': 'temperature units', 'fa': u'واحد دما'}
    dia_unit = {'en': 'diameter units', 'fa': u'واحد قطر'}
    speed_unit = {'en': 'speed units', 'fa': u'واحد سرعت'}
    table_settings = {'en': 'table settings', 'fa': u'تنظیمات جداول'}
    points_table = {'en': 'points table', 'fa': u'جدول گره‌ها'}
    table_name = {'en': 'table name', 'fa': u'نام جدول'}
    consumption_field = {'en': 'consumption field', 'fa': u'فیلد مصرف'}
    is_src_field = {'en': 'is source field', 'fa': u'فیلد ورودی'}
    pressure_field = {'en': 'pressure field', 'fa': u'فیلد فشار'}
    section_field = {'en': 'section field', 'fa': u'فیلد ناحیه'}
    pipes_table = {'en': 'pipes table', 'fa': u'جدول لوله‌ها'}
    flow_field = {'en': 'flow field', 'fa': u'فیلد دبی حجمی'}
    speed_field = {'en': 'speed field', 'fa': u'فیلد سرعت'}
    start_node_field = {'en': 'start node field', 'fa': u'فیلد گره ابتدا'}
    end_node_field = {'en': 'end node field', 'fa': u'فیلد گره انتها'}
    length_field = {'en': 'length field', 'fa': u'فیلد طول'}
    dia_field = {'en': 'diameter field', 'fa': u'فیلد قطر'}
    active_field = {'en': 'is_active field', 'fa': u'فعال بودن'}
    reset_defaults = {'en': 'reset defaults', 'fa': u'بازگشت تنظیمات اولیه'}

    empty_selection = {'en': 'Nothing is selected.', 'fa': u'چیزی انتخاب نشده است.'}
    run_error = {'en': "error is occurred during calculations", 'fa': u'خطایی در هنگام محاسبات رخ داد.'}
    run_finish = {'en': "calculations finished successfully.", 'fa': u'محاسبات با موفقیت تمام شد.'}
    empty_pressure = {
        'en': "there are some points with empty pressure value, calculating pipeline may fix this problem.",
        'fa': u"نقاطی با فشار خالی پیدا شد، اجرای محاسبات هیدرولیک ممکن است مشکل را حل کند"
    }
    empty_section = {
        'en': "there are some points with empty section value, calculating pipeline may fix this problem.",
        'fa': u"نقاطی با ناحیه بندی خالی پیدا شد، اجرای محاسبات هیدرولیک ممکن است مشکل را حل کند"
    }
    empty_speed = {
        'en': "there are some pipes with empty speed value, calculating pipeline may fix this problem.",
        'fa': u"لوله‌هایی با سرعت خالی پیدا شد، اجرای محاسبات هیدرولیک ممکن است مشکل را حل کند"
    }
    system_check = {
        'en': 'system check identified {} problems! {}',
        'fa': u'بعد از چک کردن {} مشکل پیدا شد! {}',
    }
    solve_problems = {
        'en': 'use nextProblem to find problems',
        'fa': u'برای مشاهده مشکلات، دکمه مشکل بعدی را بزنید.',
    }
    no_point_at_both_ends = {
        'en': 'no point at both end of pipe.',
        'fa': u'هیچ گره‌ای در دو طرف لوله نیست.',
    }
    no_point_at_start = {
        'en': 'no point at the start of pipe.',
        'fa': u'هیچ گره‌ای در ابتدای لوله نیست.',
    }
    no_point_at_end = {
        'en': 'no point at the end of pipe.',
        'fa': u'هیچ گره‌ای در انتهای لوله نیست.',
    }
    point_num_not_2 = {
        'en': 'number of points at the end of pipe is not 2.',
        'fa': u'در ابتدا یا انتهای لوله گره وجود ندارد و یا چند گره روی هم افتاده است.',
    }
    pipe_input_not_complete = {
        'en': 'required input is not complete for this pipe.',
        'fa': u'ورودی‌های مورد نیاز برای این لوله کامل نشده است.',
    }
    free_point = {
        'en': 'this point is not attached to a pipe.',
        'fa': u'این گره به هیچ لوله‌ای متصل نشده است.',
    }
    point_input_not_complete = {
        'en': 'required input is not complete for this point.',
        'fa': u'ورودی‌های مورد نیاز برای این گره کامل نشده است.',
    }
    no_problem = {
        'en': 'no more problems.',
        'fa': u'مشکل دیگری نیست.',
    }
    multiple_points = {
        'en': 'multiple points at the same location with ids {}',
        'fa': u'گره‌های با شماره {} در یک نقطه هستند',
    }
    not_save_error = {
        'en': "Please first save the mxd file somewhere so we can save settings.",
        'fa': u'لطفا ابتدا فایل را ذخیره کنید.'
    }
    layer_name_error = {
        'en': "Please specify {} correct name. you can change this name in settings.",
        'fa': u'لطفا نام {} را مشخص کنید. میتوانید این نام را در تنظیمات تغییر دهید.',
    }
    energy_balance_violation = {
        'en': "Energy Balance violation, can't calculate point: {} pressure.",
        'fa': u'خطا در موازنه انرژی، فشار گره {} قابل محاسبه نیست.',
    }
    mass_balance_violation = {
        'en': "Mass Balance violation, in point: {} pressure",
        'fa': u'خطا در موازنه جرم، موازنه جرم در گره {} برقرار نیست.',
    }
    unknown_diameter = {
        'en': "pipe diameter {} is not defined.",
        'fa': u'قطر {} در تنظیمات تعریف نشده است.',
    }
    no_source = {
        'en': "segment with points: {} should have at least one source!",
        'fa': u'منبع انتخاب نشده، یکی از نقاط {} باید به عنوان منبع انتخاب شود.',
    }
    invalid_licence = {
        'en': "your licence is not valid.",
        'fa': u'خطا در بررسی لایسنس نرم‌افزار',
    }
    invalid_licence = {
        'en': "your licence is not valid.",
        'fa': u'خطا در بررسی لایسنس نرم‌افزار',
    }
    layer_not_found = {
        'en': 'layer \"{}\" not found. Do you want to create it in {}?',
        'fa': u'لایه‌‌ی \"{}\" پیدا نشد، آیا میخواهید آن را در پایگاه داده‌ی\n{}\n ایجاد کنید؟',
    }
    layer_not_found_exception = {
        'en': 'layer \"{}\" not found.',
        'fa': u'لایه‌‌ی \"{}\" پیدا نشد.',
    }
    db_not_found = {
        'en': 'No Geo Database. Do you want to create a new one?',
        'fa': u'هیچ پایگاه داده‌ای وجود ندارد آیا میخواهید یک پایگاه جدید بسازید؟',
    }
    db_not_found_exception = {
        'en': 'No Geo Database.',
        'fa': u'پایگاه داده‌ای وجود ندارد.',
    }
    spatial_refrence_not_found = {
        'en': 'no spatial refrence is defined for this project. do you want to select \n{}\n refrnce by default?',
        'fa': u'دستگاه مختصاتی برای این پروژه تعریف نشده است، دستگاه \n{}\n به صورت پیشفرض انتخاب شود؟',
    }
    spatial_refrence_not_found_exception = {
        'en': 'no spatial refrence is defined.',
        'fa': u'دستگاه مختصات تعریف نشده است.',
    }
    mxd_not_saved = {
        'en': 'mxd file is not saved. please first save this file.',
        'fa': u'فایل آرک مپ هنوز ذخیره نشده است. ابتدا این فایل را ذخیره کنید.',
    }
    file_in_use_error = {
        'en': "Could not save report. The file is allready in use by another program.",
        'fa': u'خطا در ذخیره فایل. فایل در یک نرم افزار دیگر باز شده است.'
    }
    connected_source = {
        'en': "you can't connect to source. \n{}",
        'fa': u'نباید دو سورس را به یکدیگر متصل کرد. \n{}'
    }
    connected_node = {
        'en': "there is no node at the end of pipe {}.",
        'fa': u'در انتهای لوله {} نود فعالی وجود ندارد.'
    }
    not_finished_yet = {
        'en': "the calculation was canceled by user before completion.",
        'fa': u'انصراف از محاسبات قبل از پایان هنوز جواب تولید نشده است.'
    }
    not_feasible = {
        'en': "mass balance is not satisfied, decrease limit in these nodes {}.",
        'fa': u'میزان مصرف از محدودیت تولید بیشتر است، لازم است محدودیت {} تولید را افزایش دهید.'
    }

    def __init__(self, lang='fa'):
        self.lang = lang

    def __getattribute__(self, name):
        lang = super(Language, self).__getattribute__('lang')
        return super(Language, self).__getattribute__(name)[lang]
