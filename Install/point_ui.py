import json
import ttk
import sys
from Tkinter import Tk, StringVar
from os import path
from subprocess import Popen, PIPE
from translation import Language
LANG = Language()


class Application(ttk.Frame):
    def createWidgets(self, units):
        # id_label widget
        id_label = ttk.Label(self)
        id_label.configure(text=LANG.node_id)
        id_label.grid(column='0', row='0', sticky='e')
        # id_value widget
        id_value = ttk.Entry(self)
        id_value.configure(state='disabled', textvariable=self.binding['OID@'])
        id_value.grid(column='1', row='0')
        # section_label widget
        section_label = ttk.Label(self)
        section_label.configure(text=LANG.section)
        section_label.grid(column='0', row='1', sticky='e')
        # section_value widget
        section_value = ttk.Entry(self)
        section_value.configure(state='disabled', textvariable=self.binding['section'])
        section_value.grid(column='1', row='1')
        # is_source_label widget
        is_source_label = ttk.Label(self)
        is_source_label.configure(text=LANG.is_source)
        is_source_label.grid(column='0', row='2', sticky='e')
        # is_source_value widget
        is_source_value = ttk.Combobox(self)
        is_source_value.configure(width='17', textvariable=self.binding['is_source'])
        is_source_value['values'] = ['Station', 'Node']
        is_source_value.grid(column='1', row='2')
        # generation_limit_label widget
        generation_limit_label = ttk.Label(self)
        generation_limit_label.configure(text=LANG.generation_limit)
        generation_limit_label.grid(column='0', row='3', sticky='e')
        # generation_limit_value widget
        self.generation_limit_value = ttk.Entry(self)
        self.generation_limit_value.configure(textvariable=self.binding['gen_limit'])
        self.generation_limit_value.grid(column='1', row='3')
        # generation_limit_unit widget
        generation_limit_unit = ttk.Label(self)
        generation_limit_unit.configure(text=units['flow_unit'])
        generation_limit_unit.grid(column='2', row='3', sticky='w')
        # generation_label widget
        generation_label = ttk.Label(self)
        generation_label.configure(text=LANG.generation)
        generation_label.grid(column='0', row='4', sticky='e')
        # generation_value widget
        generation_value = ttk.Entry(self)
        generation_value.configure(textvariable=self.binding['generation'], state="disabled")
        generation_value.grid(column='1', row='4')
        # generation_unit widget
        generation_unit = ttk.Label(self)
        generation_unit.configure(text=units['flow_unit'])
        generation_unit.grid(column='2', row='4', sticky='w')
        # pressure_label widget
        pressure_label = ttk.Label(self)
        pressure_label.configure(text=LANG.pressure)
        pressure_label.grid(column='0', row='5', sticky='e')
        # pressure_value widget
        self.pressure_value = ttk.Entry(self)
        self.pressure_value.configure(textvariable=self.binding['pressure'])
        self.pressure_value.grid(column='1', row='5')
        # pressure_unit widget
        pressure_unit = ttk.Label(self)
        pressure_unit.configure(text=units['pressure_unit'])
        pressure_unit.grid(column='2', row='5', sticky='w')
        # consume_label widget
        consume_label = ttk.Label(self)
        consume_label.configure(compound='top', takefocus='true', text=LANG.consumption)
        consume_label.grid(column='0', row='6', sticky='e')
        # consume_value widget
        consume_value = ttk.Entry(self)
        consume_value.configure(textvariable=self.binding['consumption'])
        consume_value.grid(column='1', row='6')
        # consume_unit widget
        consume_unit = ttk.Label(self)
        consume_unit.configure(text=units['flow_unit'])
        consume_unit.grid(column='2', row='6', sticky='w')
        # is_special_label widget
        is_special_label = ttk.Label(self)
        is_special_label.configure(text=LANG.is_special)
        is_special_label.grid(column='0', row='7', sticky='e')
        # is_special_value widget
        is_special_value = ttk.Combobox(self)
        is_special_value.configure(width='17', textvariable=self.binding['is_special'])
        is_special_value['values'] = ['True', 'False']
        is_special_value.grid(column='1', row='7')
        # is_active_label widget
        is_active_label = ttk.Label(self)
        is_active_label.configure(text=LANG.is_active)
        is_active_label.grid(column='0', row='8', sticky='e')
        # is_active_value widget
        is_active_value = ttk.Combobox(self)
        is_active_value.configure(width='17', textvariable=self.binding['is_active_point'])
        is_active_value['values'] = ['True', 'False']
        is_active_value.grid(column='1', row='8')
        # save_btn widget
        save_btn = ttk.Button(self, command=self.updateData)
        save_btn.configure(text=LANG.save)
        save_btn.grid(column='1', row='9')
        self.grid(column='0', row='0', sticky='nesw')
        self.rowconfigure(0, minsize='30')
        self.rowconfigure(1, minsize='30')
        self.rowconfigure(2, minsize='30')
        self.rowconfigure(3, minsize='30')
        self.rowconfigure(4, minsize='30')
        self.rowconfigure(5, minsize='30')
        self.rowconfigure(6, minsize='30')
        self.rowconfigure(7, minsize='30')
        self.rowconfigure(8, minsize='30')
        self.rowconfigure(9, minsize='30')
        self.columnconfigure(0, minsize='0')
        self.columnconfigure(1, minsize='200')

    def __init__(self, master=None, data=None):
        ttk.Frame.__init__(self, master)
        self.configure(height='200', padding='20', width='200')
        self.binding = {
            'OID@': StringVar(),
            'consumption': StringVar(),
            'pressure': StringVar(),
            'is_source': StringVar(),
            'section': StringVar(),
            'is_active_point': StringVar(),
            'is_special': StringVar(),
            'generation': StringVar(),
            'gen_limit': StringVar(),
        }
        self.read_only_fileds = {'generation'}
        self.binding['is_source'].trace("w", lambda name, index, mode: self.on_is_source_change())
        self.createWidgets(data['units'])
        self.initialize_widgets(data['point'])

    def initialize_widgets(self, data):
        for k, v in self.binding.iteritems():
            val = data[k] if data.get(k, None) is not None else ''
            if k in self.read_only_fileds and val:
                val = round(val * 100) / 100
            v.set(val)
        self.binding['is_source'].set("Station" if data.get('is_source', False) else "Node")
        self.binding['is_active_point'].set("True" if data.get('is_active_point', True) else "False")
        self.binding['is_special'].set("True" if data.get('is_special', False) else "False")

    def updateData(self):
        res = {k: v.get() if v.get() != '' else None for k, v in self.binding.iteritems() if k not in self.read_only_fileds}
        res['is_source'] = 1 if res['is_source'] == "Station" else 0
        res['is_active_point'] = 1 if res['is_active_point'] == "True" else 0
        res['is_special'] = 1 if res['is_special'] == "True" else 0
        print(json.dumps(res))
        self.quit()

    def on_is_source_change(self):
        is_source = self.binding['is_source'].get() == "Station"
        if is_source:
            self.pressure_value['state'] = "normal"
            self.generation_limit_value['state'] = "normal"
            self.read_only_fileds -= {'pressure', 'gen_limit'}
        else:
            self.pressure_value['state'] = "disabled"
            self.generation_limit_value['state'] = "disabled"
            self.read_only_fileds |= {'pressure', 'gen_limit'}

def main():
    root = Tk()
    root.title(LANG.point_ui)
    data = json.loads(sys.argv[1]) if len(sys.argv) > 1 else None
    if data is None:
        data = {
            'units': {'pressure_unit': '', 'flow_unit': ''},
            'point': {}
        }
    app = Application(master=root, data=data)
    app.mainloop()
    try:
        root.destroy()
    except:
        pass


def exec_in_sub_process(data):
    file_path = path.abspath(__file__)
    args = json.dumps(data)
    proc = Popen([file_path, args], shell=True, stdout=PIPE, stderr=PIPE, bufsize=1)
    stdoutdata, stderrdata = proc.communicate()
    proc.terminate()
    return stdoutdata.strip()


if __name__ == '__main__':
    main()
