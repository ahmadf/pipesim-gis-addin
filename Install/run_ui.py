import json
import ttk
import sys
from Tkinter import Tk, StringVar, PhotoImage

from os import path
from subprocess import Popen, PIPE
from translation import Language
LANG = Language()


class Application(ttk.Frame):
    def createWidgets(self):
        # left section
        left_section = ttk.Frame(self)
        # software widget
        software = ttk.Labelframe(left_section)
        software.configure(height='200', text=LANG.software_type, width='200')
        # stand alone
        stand_alone = ttk.Radiobutton(software)
        stand_alone.configure(text='Stand Alone', variable=self.selected_software, value='stand_alone')
        stand_alone.grid(column='0', row='0', sticky='w')
        # network
        network = ttk.Radiobutton(software)
        network.configure(text='Network', variable=self.selected_software, value='network')
        network.grid(column='0', row='1', sticky='w')
        # network_label
        net_label = ttk.Label(software)
        net_label.configure(text="Sever Address")
        net_label.grid(column='0', row='2')
        # net_value widget
        self.net_value = ttk.Entry(software)
        self.net_value.configure(textvariable=self.network_address, state='disabled')
        self.net_value.grid(column='1', row='2')
        software.grid(column='0', row='0', sticky='w')
        software.columnconfigure(0, minsize='150')

        # formula widget
        formula = ttk.Labelframe(left_section)
        formula.configure(height='200', text=LANG.select_formula, width='200')
        # igt widget
        igt = ttk.Radiobutton(formula)
        igt.configure(text='IGT', variable=self.selected_method, value='igt')
        igt.grid(column='0', row='0', sticky='w')
        # Penhandle_A widget
        Penhandle_A = ttk.Radiobutton(formula)
        Penhandle_A.configure(text='Penhandle A', variable=self.selected_method, value='pen_A')
        Penhandle_A.grid(column='0', row='1', sticky='w')
        # Penhandle_B widget
        Penhandle_B = ttk.Radiobutton(formula)
        Penhandle_B.configure(text='Penhandle B', variable=self.selected_method, value='pen_B')
        Penhandle_B.grid(column='0', row='2', sticky='w')
        # Weymouth widget
        Weymouth = ttk.Radiobutton(formula)
        Weymouth.configure(text='Weymouth', variable=self.selected_method, value='wey')
        Weymouth.grid(column='0', row='3', sticky='w')
        # Spitglass_HP widget
        Spitglass_HP = ttk.Radiobutton(formula)
        Spitglass_HP.configure(text='Splitglass HP', variable=self.selected_method, value='sgh')
        Spitglass_HP.grid(column='0', row='4', sticky='w')
        formula.grid(column='0', row='1', sticky='w')
        formula.columnconfigure(0, minsize='150')

        left_section.grid(column='0', row='0')
        left_section.columnconfigure(0, minsize='150')
        # calculate widget
        calculate = ttk.Button(self, command=self.execute)
        calculate.configure(text=LANG.calculate)
        calculate.grid(column='0', row='1')
        self.formula_image = ttk.Label(self)
        self.formula_image.configure(image=IMAGES['igt'])
        self.formula_image.grid(column='1', row='0')
        self.grid(column='0', row='0')
        self.rowconfigure(1, minsize='40')
        self.columnconfigure(0, pad='5')

    def __init__(self, master=None, data={}):
        ttk.Frame.__init__(self, master)
        self.configure(height='200', padding='20', width='200')
        global IMAGES
        IMAGES = {
            'igt': PhotoImage(file=path.dirname(__file__) + r"/images/igt.gif"),
            'pen_A': PhotoImage(file=path.dirname(__file__) + r"/images/pen_A.gif"),
            'pen_B': PhotoImage(file=path.dirname(__file__) + r"/images/pen_B.gif"),
            'wey': PhotoImage(file=path.dirname(__file__) + r"/images/wey.gif"),
            'sgh': PhotoImage(file=path.dirname(__file__) + r"/images/sgh.gif"),
        }
        self.selected_method = StringVar()
        self.selected_method.set("igt")
        self.selected_method.trace("w", lambda name, index, mode: self.on_method_change())

        self.selected_software = StringVar()
        self.selected_software.set("stand_alone")
        self.selected_software.trace("w", lambda name, index, mode: self.on_software_change())

        self.network_address = StringVar()
        self.network_address.set("localhost:8000")

        self.createWidgets()

    def on_method_change(self):
        method = self.selected_method.get()
        self.formula_image.configure(image=IMAGES[method])

    def execute(self):
        res = {'method': self.selected_method.get(), 'software': self.selected_software.get(), 'server_address': self.network_address.get()}
        print(json.dumps(res))
        self.quit()

    def on_software_change(self):
        is_network = self.selected_software.get() == "network"
        if is_network:
            self.net_value['state'] = "normal"
        else:
            self.net_value['state'] = "disabled"


def main():
    root = Tk()
    root.title(LANG.run_ui)
    data = json.loads(sys.argv[1] if len(sys.argv) > 1 else '{}')
    app = Application(master=root, data=data)
    app.mainloop()
    try:
        root.destroy()
    except:
        pass


def exec_in_sub_process():
    file_path = path.abspath(__file__)
    proc = Popen(file_path, shell=True, stdout=PIPE, stderr=PIPE, bufsize=1)
    stdoutdata, stderrdata = proc.communicate()
    proc.terminate()
    return stdoutdata.strip()


if __name__ == '__main__':
    main()
