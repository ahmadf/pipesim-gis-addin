# -*- coding: utf-8 -*-
import json
import sys

from os import path, devnull
from subprocess import Popen, PIPE

from pipe_steady.models import GasProperty, PipeLine, Settings, Logger
from pipe_steady.engine import SteadySolver
from translation import Language
LANG = Language()


def calculate_pipeline(body, file_address):
    points = body['points']
    streams = body['pipes']
    opt = body.get('settings', {})

    setting = Settings(method=body['method'], mod_factor=float(opt['constants']['cf']), temperature_unit=opt['units']['temperature_unit'],
                       pressure_unit=opt['units']['pressure_unit'], length_unit=opt['units']['length_unit'],
                       diameter_unit=opt['units']['diameter_unit'], flow_unit=opt['units']['flow_unit'], use_nps=opt['use_nps'],
                       diameter_table={int(row[0]): float(row[1]) for row in opt['nps_dict']})
    gas_property = GasProperty(gravity=float(opt['constants']['ro']), temp=float(opt['constants']['t']), z=float(opt['constants'].get('Z', 1.0)), eff=float(opt['constants']['E']),
                               Tb_Pb=float(opt['constants']['t0']) / float(opt['constants']['p0']), viscosity=float(opt['constants']['vis']))
    try:
        pls = PipeLine.find_all_pipelines(points, streams, setting)
    except AssertionError as e:
        return {'error': e.message}

    Logger.instance().set_total(len(pls), logger_type='file', file_address=file_address)
    flows = {}
    speeds = {}
    pressures = {}
    sections = {}
    gen = {}
    for i, pl in enumerate(pls):
        done = False
        solver = SteadySolver(pl, gas_property)
        Logger.instance().next_state()
        while not done:
            Logger.instance().progress = 10
            Logger.instance().nonlin_count = 0
            Logger.instance().nonlin_x = 0
            solver.hardy_cross(Logger.instance().update_nonlin_progress)
            if pl.check_limits():
                done = True

        Logger.instance().next_state()
        solver.find_points_pressure()
        Logger.instance().next_state()
        result = pl.generate_response(setting)
        flows.update(result['flow'])
        speeds.update(result['speed'])
        pressures.update(result['pressure'])
        sections.update({p.id: i + 1 for p in pl.points})
        gen.update(result['generation'])

    return {'flows': flows, 'speeds': speeds, 'pressures': pressures, 'sections': sections, 'gen': gen}


def calculate_pipeline_in_sub_process(body):
    from helper import DataStore
    try:
        file_address = DataStore.instance().file_address
    except RuntimeError:
        file_address = 'out.txt'
    open(file_address, 'w').close()
    file_path = path.abspath(__file__)
    data = json.dumps(body)
    file = "{} \"{}\"".format(file_path, file_address.encode('utf'))
    with open(devnull, 'w') as dnull:
        p = Popen(file, shell=True, stdin=PIPE, stderr=dnull, bufsize=1)
    p.stdin.write(data)
    p.stdin.close()
    return file_address, p


def main():
    for line in sys.stdin:
        data = line
    data = json.loads(data)
    file_address = sys.argv[1].decode('utf8')
    result = calculate_pipeline(data, file_address)
    with open(file_address, 'a') as f:
        json.dump(result, f)


if __name__ == '__main__':
    main()
