import json
import ttk
import sys
from Tkinter import Tk, StringVar, IntVar
from os import path
from subprocess import Popen, PIPE
from translation import Language
LANG = Language()


class Application(ttk.Frame):
    def createWidgets(self):
        # constants widget
        constants = ttk.Labelframe(self)
        constants.configure(height='200', text=LANG.constants, width='200')
        # cf_label widget
        cf_label = ttk.Label(constants)
        cf_label.configure(text=LANG.cf)
        cf_label.grid(column='0', row='0', sticky='e')
        # cf_value widget
        cf_value = ttk.Entry(constants)
        cf_value.configure(textvariable=self.binding['cf'])
        cf_value.grid(column='1', row='0')
        # E_label widget
        E_label = ttk.Label(constants)
        E_label.configure(text=LANG.E)
        E_label.grid(column='0', row='1', sticky='e')
        # E_value widget
        E_value = ttk.Entry(constants)
        E_value.configure(textvariable=self.binding['E'])
        E_value.grid(column='1', row='1')
        # t0_label widget
        t0_label = ttk.Label(constants)
        t0_label.configure(text=LANG.t0)
        t0_label.grid(column='0', row='2', sticky='e')
        # t0_value widget
        t0_value = ttk.Entry(constants)
        t0_value.configure(textvariable=self.binding['t0'])
        t0_value.grid(column='1', row='2')
        # p0_label widget
        p0_label = ttk.Label(constants)
        p0_label.configure(text=LANG.p0)
        p0_label.grid(column='0', row='3', sticky='e')
        # p0_value widget
        p0_value = ttk.Entry(constants)
        p0_value.configure(textvariable=self.binding['p0'])
        p0_value.grid(column='1', row='3')
        # t_label widget
        t_label = ttk.Label(constants)
        t_label.configure(text=LANG.t)
        t_label.grid(column='0', row='4', sticky='e')
        # t_value widget
        t_value = ttk.Entry(constants)
        t_value.configure(textvariable=self.binding['t'])
        t_value.grid(column='1', row='4')
        # ro_label widget
        ro_label = ttk.Label(constants)
        ro_label.configure(text=LANG.ro)
        ro_label.grid(column='0', row='5', sticky='e')
        # ro_value widget
        ro_value = ttk.Entry(constants)
        ro_value.configure(textvariable=self.binding['ro'])
        ro_value.grid(column='1', row='5')
        # vis_label widget
        vis_label = ttk.Label(constants)
        vis_label.configure(text=LANG.vis)
        vis_label.grid(column='0', row='6', sticky='e')
        # vis_value widget
        vis_value = ttk.Entry(constants)
        vis_value.configure(textvariable=self.binding['vis'])
        vis_value.grid(column='1', row='6')
        constants.grid(column='0', row='0', sticky='n')
        constants.rowconfigure(1, minsize='30')
        constants.rowconfigure(0, minsize='30')
        constants.rowconfigure(3, minsize='30')
        constants.rowconfigure(2, minsize='30')
        constants.rowconfigure(5, minsize='30')
        constants.rowconfigure(4, minsize='30')
        constants.rowconfigure(6, minsize='30')
        constants.columnconfigure(0, minsize='40')
        constants.columnconfigure(1, minsize='150')
        # pipe_settings widget
        self.pipe_settings = ttk.Labelframe(self)
        self.pipe_settings.configure(height='200', text=LANG.pipe_settings, width='200')
        # nps_number_label widget
        nps_number_label = ttk.Label(self.pipe_settings)
        nps_number_label.configure(text=LANG.nps_number)
        nps_number_label.grid(column='0', row='0', sticky='e')
        # nps_number_value widget
        add_nps_btn = ttk.Button(self.pipe_settings, command=self.add_nps_entry)
        add_nps_btn.configure(text='+')
        add_nps_btn.grid(column='1', row='0')
        remove_nps_btn = ttk.Button(self.pipe_settings, command=self.remove_nps_entry)
        remove_nps_btn.configure(text='-')
        remove_nps_btn.grid(column='0', row='0')
        self.pipe_settings.grid(column='1', row='0', sticky='n')
        self.pipe_settings.rowconfigure(0, minsize='50')
        self.pipe_settings.columnconfigure(0, minsize='40')
        # unit_settings widget
        unit_settings = ttk.Labelframe(self)
        unit_settings.configure(height='200', text=LANG.unit_settings, width='200')
        # flow_units widget
        flow_units = ttk.Labelframe(unit_settings)
        flow_units.configure(height='200', text=LANG.flow_unit, width='200')
        # SCMD widget
        SCMD = ttk.Radiobutton(flow_units)
        SCMD.configure(text='SCMD', variable=self.binding['flow_unit'], value="SCMD")
        SCMD.grid(column='0', row='0', sticky='w')
        # SCMH widget
        SCMH = ttk.Radiobutton(flow_units)
        SCMH.configure(text='SCMH', variable=self.binding['flow_unit'], value="SCMH")
        SCMH.grid(column='0', row='1', sticky='w')
        # MSCMD widget
        MSCMD = ttk.Radiobutton(flow_units)
        MSCMD.configure(text='MSCMD', variable=self.binding['flow_unit'], value="MSCMD")
        MSCMD.grid(column='0', row='2', sticky='w')
        # SCFD widget
        SCFD = ttk.Radiobutton(flow_units)
        SCFD.configure(text='SCFD', variable=self.binding['flow_unit'], value="SCFD")
        SCFD.grid(column='1', row='0', sticky='w')
        # SCFH widget
        SCFH = ttk.Radiobutton(flow_units)
        SCFH.configure(text='SCFH', variable=self.binding['flow_unit'], value="SCFH")
        SCFH.grid(column='1', row='1', sticky='w')
        # MMSCFD widget
        MMSCFD = ttk.Radiobutton(flow_units)
        MMSCFD.configure(text='MMSCFD', variable=self.binding['flow_unit'], value="MMSCFD")
        MMSCFD.grid(column='1', row='2', sticky='w')
        flow_units.grid(column='0', row='0')
        flow_units.columnconfigure(0, minsize='80')
        flow_units.columnconfigure(1, minsize='80')
        # length_units widget
        length_units = ttk.Labelframe(unit_settings)
        length_units.configure(height='200', text=LANG.length_unit, width='200')
        # m widget
        m = ttk.Radiobutton(length_units)
        m.configure(text='m', variable=self.binding['length_unit'], value="m")
        m.grid(column='0', row='0', sticky='w')
        # km widget
        km = ttk.Radiobutton(length_units)
        km.configure(text='km', variable=self.binding['length_unit'], value="km")
        km.grid(column='0', row='1', sticky='w')
        # ft widget
        ft = ttk.Radiobutton(length_units)
        ft.configure(text='ft', variable=self.binding['length_unit'], value="ft")
        ft.grid(column='1', row='0', sticky='w')
        # mile widget
        mile = ttk.Radiobutton(length_units)
        mile.configure(text='mile', variable=self.binding['length_unit'], value="mile")
        mile.grid(column='1', row='1', sticky='w')
        length_units.grid(column='0', row='1')
        length_units.columnconfigure(0, minsize='80')
        length_units.columnconfigure(1, minsize='80')
        # pressure_units widget
        pressure_units = ttk.Labelframe(unit_settings)
        pressure_units.configure(height='200', text=LANG.pressure_unit, width='200')
        # bar widget
        bar = ttk.Radiobutton(pressure_units)
        bar.configure(text='bar', variable=self.binding['pressure_unit'], value="bar")
        bar.grid(column='0', row='0', sticky='w')
        # barg widget
        barg = ttk.Radiobutton(pressure_units)
        barg.configure(text='barg', variable=self.binding['pressure_unit'], value="barg")
        barg.grid(column='1', row='0', sticky='w')
        # psi widget
        psi = ttk.Radiobutton(pressure_units)
        psi.configure(text='psi', variable=self.binding['pressure_unit'], value="psi")
        psi.grid(column='0', row='1', sticky='w')
        # psig widget
        psig = ttk.Radiobutton(pressure_units)
        psig.configure(text='psig', variable=self.binding['pressure_unit'], value="psig")
        psig.grid(column='1', row='1', sticky='w')
        # kPa widget
        kPa = ttk.Radiobutton(pressure_units)
        kPa.configure(text='kPa', variable=self.binding['pressure_unit'], value="kPa")
        kPa.grid(column='0', row='2', sticky='w')
        # kPag widget
        kPag = ttk.Radiobutton(pressure_units)
        kPag.configure(text='kPag', variable=self.binding['pressure_unit'], value="kPag")
        kPag.grid(column='1', row='2', sticky='w')
        pressure_units.grid(column='0', row='2')
        pressure_units.columnconfigure(0, minsize='80')
        pressure_units.columnconfigure(1, minsize='80')
        # temperature_units widget
        temperature_units = ttk.Labelframe(unit_settings)
        temperature_units.configure(height='200', text=LANG.temp_unit, width='200')
        # C widget
        C = ttk.Radiobutton(temperature_units)
        C.configure(text='C', variable=self.binding['temperature_unit'], value="C")
        C.grid(column='0', row='0', sticky='w')
        # K widget
        K = ttk.Radiobutton(temperature_units)
        K.configure(text='K', variable=self.binding['temperature_unit'], value="K")
        K.grid(column='0', row='1', sticky='w')
        # F widget
        F = ttk.Radiobutton(temperature_units)
        F.configure(text='F', variable=self.binding['temperature_unit'], value="F")
        F.grid(column='1', row='0', sticky='w')
        # R widget
        R = ttk.Radiobutton(temperature_units)
        R.configure(text='R', variable=self.binding['temperature_unit'], value="R")
        R.grid(column='1', row='1', sticky='w')
        temperature_units.grid(column='0', row='3')
        temperature_units.columnconfigure(0, minsize='80')
        temperature_units.columnconfigure(1, minsize='80')
        # diameter_units widget
        diameter_units = ttk.Labelframe(unit_settings)
        diameter_units.configure(height='200', text=LANG.dia_unit, width='200')
        # cm widget
        cm = ttk.Radiobutton(diameter_units)
        cm.configure(text='cm', variable=self.binding['diameter_unit'], value="cm")
        cm.grid(column='0', row='0', sticky='w')
        # In widget
        In = ttk.Radiobutton(diameter_units)
        In.configure(text='In', variable=self.binding['diameter_unit'], value="In")
        In.grid(column='1', row='0', sticky='w')
        diameter_units.grid(column='0', row='4')
        diameter_units.columnconfigure(0, minsize='80')
        diameter_units.columnconfigure(1, minsize='80')
        # speed_units widget
        speed_units = ttk.Labelframe(unit_settings)
        speed_units.configure(height='200', text=LANG.speed_unit, width='200')
        # m_s widget
        m_s = ttk.Radiobutton(speed_units)
        m_s.configure(text='m/s', variable=self.binding['speed_unit'], value="m/s")
        m_s.grid(column='0', row='0', sticky='w')
        # ft_s widget
        ft_s = ttk.Radiobutton(speed_units)
        ft_s.configure(text='ft/s', variable=self.binding['speed_unit'], value="ft/s")
        ft_s.grid(column='1', row='0', sticky='w')
        speed_units.grid(column='0', row='5')
        speed_units.columnconfigure(0, minsize='80')
        speed_units.columnconfigure(1, minsize='80')
        unit_settings.grid(column='2', row='0', sticky='n')
        unit_settings.columnconfigure(0, minsize='0', pad='5')
        # general_settings widget
        general_settings = ttk.Labelframe(self)
        general_settings.configure(height='200', text=LANG.table_settings, width='200')
        # points_settings widget
        points_settings = ttk.Labelframe(general_settings)
        points_settings.configure(height='200', text=LANG.points_table, width='200')
        # nlayer_name_label widget
        nlayer_name_label = ttk.Label(points_settings)
        nlayer_name_label.configure(text=LANG.table_name)
        nlayer_name_label.grid(column='0', row='0', sticky='e')
        # nlayer_name_value widget
        nlayer_name_value = ttk.Entry(points_settings)
        nlayer_name_value.configure(textvariable=self.binding['point_layer_name'])
        nlayer_name_value.grid(column='1', row='0')
        # con_field_label widget
        con_field_label = ttk.Label(points_settings)
        con_field_label.configure(text=LANG.consumption_field)
        con_field_label.grid(column='0', row='1', sticky='e')
        # con_field_value widget
        con_field_value = ttk.Entry(points_settings)
        con_field_value.configure(textvariable=self.binding['consumption'])
        con_field_value.grid(column='1', row='1')
        # is_source_label widget
        is_source_label = ttk.Label(points_settings)
        is_source_label.configure(text=LANG.is_src_field)
        is_source_label.grid(column='0', row='2', sticky='e')
        # is_source_value widget
        is_source_value = ttk.Entry(points_settings)
        is_source_value.configure(textvariable=self.binding['is_source'])
        is_source_value.grid(column='1', row='2')
        # pressure_field_label widget
        pressure_field_label = ttk.Label(points_settings)
        pressure_field_label.configure(text=LANG.pressure_field)
        pressure_field_label.grid(column='0', row='3', sticky='e')
        # pressure_field_value widget
        pressure_field_value = ttk.Entry(points_settings)
        pressure_field_value.configure(textvariable=self.binding['pressure'])
        pressure_field_value.grid(column='1', row='3')
        # section_field_label widget
        section_field_label = ttk.Label(points_settings)
        section_field_label.configure(text=LANG.section_field)
        section_field_label.grid(column='0', row='4', sticky='e')
        # section_field_value widget
        section_field_value = ttk.Entry(points_settings)
        section_field_value.configure(textvariable=self.binding['section'])
        section_field_value.grid(column='1', row='4')
        # is_active_field_label widget
        is_active_field_label2 = ttk.Label(points_settings)
        is_active_field_label2.configure(text=LANG.active_field)
        is_active_field_label2.grid(column='0', row='5', sticky='e')
        # is_active_field_value widget
        is_active_field_value2 = ttk.Entry(points_settings)
        is_active_field_value2.configure(textvariable=self.binding['is_active_point'])
        is_active_field_value2.grid(column='1', row='5')
        # is_special_label widget
        is_special_field_label = ttk.Label(points_settings)
        is_special_field_label.configure(text=LANG.is_special)
        is_special_field_label.grid(column='0', row='6', sticky='e')
        # is_special_value widget
        is_special_field_value = ttk.Entry(points_settings)
        is_special_field_value.configure(textvariable=self.binding['is_special'])
        is_special_field_value.grid(column='1', row='6')
        # gen_limit_field_label widget
        gen_limit_field_label = ttk.Label(points_settings)
        gen_limit_field_label.configure(text=LANG.generation_limit)
        gen_limit_field_label.grid(column='0', row='7', sticky='e')
        # gen_limit_value widget
        gen_limit_value = ttk.Entry(points_settings)
        gen_limit_value.configure(textvariable=self.binding['gen_limit'])
        gen_limit_value.grid(column='1', row='7')
        # generation_field_label widget
        generation_field_label = ttk.Label(points_settings)
        generation_field_label.configure(text=LANG.generation)
        generation_field_label.grid(column='0', row='8', sticky='e')
        # generation_field_value widget
        generation_field_value = ttk.Entry(points_settings)
        generation_field_value.configure(textvariable=self.binding['generation'])
        generation_field_value.grid(column='1', row='8')
        # station_name_label widget
        station_name_label = ttk.Label(points_settings)
        station_name_label.configure(text=LANG.station_name)
        station_name_label.grid(column='0', row='9', sticky='e')
        # station_name_value widget
        station_name_value = ttk.Entry(points_settings)
        station_name_value.configure(textvariable=self.binding['station_name'])
        station_name_value.grid(column='1', row='9')

        points_settings.grid(column='0', row='0')
        points_settings.rowconfigure(0, minsize='40')
        points_settings.columnconfigure(0, minsize='105')
        # pipes_settings widget
        pipes_settings = ttk.Labelframe(general_settings)
        pipes_settings.configure(height='200', text=LANG.pipes_table, width='200')
        # elayer_name_label widget
        elayer_name_label = ttk.Label(pipes_settings)
        elayer_name_label.configure(text=LANG.table_name)
        elayer_name_label.grid(column='0', row='0', sticky='e')
        # elayer_name_value widget
        elayer_name_value = ttk.Entry(pipes_settings)
        elayer_name_value.configure(textvariable=self.binding['pipe_layer_name'])
        elayer_name_value.grid(column='1', row='0')
        # flow_field_label widget
        flow_field_label = ttk.Label(pipes_settings)
        flow_field_label.configure(text=LANG.flow_field)
        flow_field_label.grid(column='0', row='1', sticky='e')
        # flow_field_value widget
        flow_field_value = ttk.Entry(pipes_settings)
        flow_field_value.configure(textvariable=self.binding['flow'])
        flow_field_value.grid(column='1', row='1')
        # speed_field_label widget
        speed_field_label = ttk.Label(pipes_settings)
        speed_field_label.configure(text=LANG.speed_field)
        speed_field_label.grid(column='0', row='2', sticky='e')
        # speed_field_value widget
        speed_field_value = ttk.Entry(pipes_settings)
        speed_field_value.configure(textvariable=self.binding['speed'])
        speed_field_value.grid(column='1', row='2')
        # start_field_label widget
        start_field_label = ttk.Label(pipes_settings)
        start_field_label.configure(text=LANG.start_node_field)
        start_field_label.grid(column='0', row='3', sticky='e')
        # start_field_value widget
        start_field_value = ttk.Entry(pipes_settings)
        start_field_value.configure(textvariable=self.binding['start'])
        start_field_value.grid(column='1', row='3')
        # end_field_label widget
        end_field_label = ttk.Label(pipes_settings)
        end_field_label.configure(text=LANG.end_node_field)
        end_field_label.grid(column='0', row='4', sticky='e')
        # end_field_value widget
        end_field_value = ttk.Entry(pipes_settings)
        end_field_value.configure(textvariable=self.binding['end'])
        end_field_value.grid(column='1', row='4')
        # length_field_label widget
        length_field_label = ttk.Label(pipes_settings)
        length_field_label.configure(text=LANG.length_field)
        length_field_label.grid(column='0', row='5', sticky='e')
        # length_field_value widget
        length_field_value = ttk.Entry(pipes_settings)
        length_field_value.configure(textvariable=self.binding['length'])
        length_field_value.grid(column='1', row='5')
        # diameter_field_label widget
        diameter_field_label = ttk.Label(pipes_settings)
        diameter_field_label.configure(text=LANG.dia_field)
        diameter_field_label.grid(column='0', row='6', sticky='e')
        # diameter_field_value widget
        diameter_field_value = ttk.Entry(pipes_settings)
        diameter_field_value.configure(textvariable=self.binding['diameter'])
        diameter_field_value.grid(column='1', row='6')
        # is_active_field_label widget
        is_active_field_label = ttk.Label(pipes_settings)
        is_active_field_label.configure(text=LANG.active_field)
        is_active_field_label.grid(column='0', row='7', sticky='e')
        # is_active_field_value widget
        is_active_field_value = ttk.Entry(pipes_settings)
        is_active_field_value.configure(textvariable=self.binding['is_active'])
        is_active_field_value.grid(column='1', row='7')
        pipes_settings.grid(column='0', row='1')
        pipes_settings.rowconfigure(0, minsize='40')
        pipes_settings.columnconfigure(0, minsize='105')
        general_settings.grid(column='3', row='0', sticky='n')
        general_settings.columnconfigure(0, pad='5')
        # save_btn widget
        save_btn = ttk.Button(self, command=self.update_data)
        save_btn.configure(text=LANG.save)
        save_btn.grid(column='1', row='1', sticky='e')
        # reset_btn widget
        reset_btn = ttk.Button(self, command=self.reset_default)
        reset_btn.configure(text=LANG.reset_defaults)
        reset_btn.grid(column='2', row='1', sticky='w')
        self.grid(column='0', row='0')
        self.rowconfigure(1, minsize='50')
        self.rowconfigure(0, pad='0')
        self.columnconfigure(0, pad='4')
        self.columnconfigure(1, pad='4')
        self.columnconfigure(2, pad='4')
        self.columnconfigure(3, pad='4')

    def __init__(self, master=None, data={}, defaults={}):
        ttk.Frame.__init__(self, master)
        self.configure(height='200', padding='20', width='200')
        self.defaults = defaults
        self.binding = {
            'cf': StringVar(),
            'E': StringVar(),
            't0': StringVar(),
            'p0': StringVar(),
            't': StringVar(),
            'ro': StringVar(),
            'vis': StringVar(),

            'flow_unit': StringVar(),
            'speed_unit': StringVar(),
            'length_unit': StringVar(),
            'pressure_unit': StringVar(),
            'temperature_unit': StringVar(),
            'diameter_unit': StringVar(),

            'point_layer_name': StringVar(),
            'consumption': StringVar(),
            'is_source': StringVar(),
            'pressure': StringVar(),
            'section': StringVar(),
            'is_active_point': StringVar(),
            'is_special': StringVar(),
            'gen_limit': StringVar(),
            'station_name': StringVar(),
            'generation': StringVar(),

            'pipe_layer_name': StringVar(),
            'flow': StringVar(),
            'speed': StringVar(),
            'start': StringVar(),
            'end': StringVar(),
            'length': StringVar(),
            'diameter': StringVar(),
            'is_active': StringVar(),
        }
        self.nps_entries = []
        self.nps_vars = []
        self.nps_count = IntVar()
        self.nps_count.trace("w", lambda name, index, mode: self.on_nps_change())
        self.createWidgets()
        self.initialize_widgets(data)
        self.initialize_nps(data)

    def initialize_widgets(self, data):
        for k, v in self.binding.iteritems():
            if k != 'nps':
                v.set(data[k] if data.get(k, None) is not None else '')

    def initialize_nps(self, data):
        self.nps_count.set(len(data.get('nps', [])))
        for i, row in enumerate(data.get('nps', [])):
            self.nps_vars[i][0].set(row[0])
            self.nps_vars[i][1].set(row[1])

    def update_data(self):
        res = {k: v.get() if v.get() != '' else None for k, v in self.binding.iteritems()}
        res['nps'] = [[row[0].get(), row[1].get()] for row in self.nps_vars]
        print(json.dumps(res))
        self.quit()

    def reset_default(self):
        self.initialize_widgets(self.defaults)
        self.initialize_nps(self.defaults)

    def add_nps_entry(self):
        value = self.nps_count.get()
        self.nps_count.set(value + 1)

    def remove_nps_entry(self):
        value = self.nps_count.get()
        if value != 0:
            self.nps_count.set(value - 1)

    def on_nps_change(self):
        try:
            value = self.nps_count.get()
        except:
            return
        old_value = len(self.nps_entries)
        for i in range(old_value, value, -1):
            row = self.nps_entries.pop()
            self.nps_vars.pop()
            row[0].grid_forget()
            row[1].grid_forget()

        for i in range(old_value, value):
            # nps_label widget
            self.nps_entries.append([0, 0])
            self.nps_vars.append([StringVar(), StringVar()])
            self.nps_entries[i][0] = ttk.Entry(self.pipe_settings)
            self.nps_entries[i][0].configure(textvariable=self.nps_vars[i][0], width='6')
            self.nps_entries[i][0].grid(column='0', row=i + 1, sticky='e')
            # nps_value widget
            self.nps_entries[i][1] = ttk.Entry(self.pipe_settings)
            self.nps_entries[i][1].configure(textvariable=self.nps_vars[i][1])
            self.nps_entries[i][1].grid(column='1', row=i + 1, sticky='w')


def main():
    root = Tk()
    root.title(LANG.settings_ui)
    data = json.loads(sys.argv[1] if len(sys.argv) > 1 else '{}')
    defaults = json.loads(sys.argv[2] if len(sys.argv) > 1 else '{}')
    app = Application(master=root, data=data, defaults=defaults)
    app.mainloop()
    try:
        root.destroy()
    except:
        pass


def exec_in_sub_process(data, defaults):
    file_path = path.abspath(__file__)
    data = json.dumps(data)
    defaults = json.dumps(defaults)
    proc = Popen([file_path, data, defaults], shell=True, stdout=PIPE, stderr=PIPE, bufsize=1)
    stdoutdata, stderrdata = proc.communicate()
    proc.terminate()
    return stdoutdata.strip()


if __name__ == '__main__':
    main()
