import json
import ttk
import sys
from Tkinter import Tk, StringVar
from os import path
from subprocess import Popen, PIPE
from translation import Language
LANG = Language()


class Application(ttk.Frame):
    def createWidgets(self, units, npses):
        # id_label widget
        id_label = ttk.Label(self)
        id_label.configure(text=LANG.pipe_id)
        id_label.grid(column='0', row='0', sticky='e')
        # id_value widget
        id_value = ttk.Entry(self)
        id_value.configure(state='disabled', textvariable=self.binding['OID@'])
        id_value.grid(column='1', row='0')
        # from_node_label widget
        from_node_label = ttk.Label(self)
        from_node_label.configure(text=LANG.from_node)
        from_node_label.grid(column='0', row='1', sticky='e')
        # from_node_value widget
        from_node_value = ttk.Entry(self)
        from_node_value.configure(state='disabled', textvariable=self.binding['start'])
        from_node_value.grid(column='1', row='1')
        # to_node_label widget
        to_node_label = ttk.Label(self)
        to_node_label.configure(text=LANG.to_node)
        to_node_label.grid(column='2', row='1', sticky='e')
        # to_node_value widget
        to_node_value = ttk.Entry(self)
        to_node_value.configure(state='disabled', textvariable=self.binding['end'])
        to_node_value.grid(column='3', row='1')
        # length_label widget
        length_label = ttk.Label(self)
        length_label.configure(text=LANG.length)
        length_label.grid(column='0', row='3', sticky='e')
        # length_value widget
        length_value = ttk.Entry(self)
        length_value.configure(state='disabled', textvariable=self.binding['length'])
        length_value.grid(column='1', row='3')
        # length_unit widget
        length_unit = ttk.Label(self)
        length_unit.configure(text=units['length_unit'])
        length_unit.grid(column='2', row='3', sticky='w')
        # diameter_label widget
        diameter_label = ttk.Label(self)
        diameter_label.configure(text=LANG.diameter)
        diameter_label.grid(column='0', row='4', sticky='e')
        # diameter_value widget
        if npses:
            diameter_value = ttk.Combobox(self)
            diameter_value.configure(width='17', textvariable=self.binding['diameter'])
            diameter_value['values'] = npses
        else:
            diameter_value = ttk.Entry(self)
            diameter_value.configure(textvariable=self.binding['diameter'])
        diameter_value.grid(column='1', row='4')
        # is_active_label widget
        is_active_label = ttk.Label(self)
        is_active_label.configure(text=LANG.is_active)
        is_active_label.grid(column='0', row='5', sticky='e')
        # is_source_value widget
        is_active_value = ttk.Combobox(self)
        is_active_value.configure(width='17', textvariable=self.binding['is_active'])
        is_active_value['values'] = ['True', 'False']
        is_active_value.grid(column='1', row='5')
        # speed_label widget
        speed_label = ttk.Label(self)
        speed_label.configure(text=LANG.speed)
        speed_label.grid(column='0', row='6', sticky='e')
        # speed_value widget
        speed_value = ttk.Entry(self)
        speed_value.configure(state='disabled', textvariable=self.binding['speed'])
        speed_value.grid(column='1', row='6')
        # speed_unit widget
        speed_unit = ttk.Label(self)
        speed_unit.configure(text=units['speed_unit'])
        speed_unit.grid(column='2', row='6', sticky='w')
        # flow_label widget
        flow_label = ttk.Label(self)
        flow_label.configure(text=LANG.flow)
        flow_label.grid(column='0', row='7', sticky='e')
        # flow_value widget
        flow_value = ttk.Entry(self)
        flow_value.configure(state='disabled', textvariable=self.binding['flow'])
        flow_value.grid(column='1', row='7')
        # flow_unit widget
        flow_unit = ttk.Label(self)
        flow_unit.configure(text=units['flow_unit'])
        flow_unit.grid(column='2', row='7', sticky='w')
        # save_btn widget
        save_btn = ttk.Button(self, command=self.updateData)
        save_btn.configure(text=LANG.save)
        save_btn.grid(column='1', row='8')
        # connection_btn widget
        connection_btn = ttk.Button(self, command=self.updateConnection)
        connection_btn.configure(text=LANG.edit_connection)
        connection_btn.grid(column='3', row='2')
        self.grid(column='0', row='0', sticky='nesw')
        self.rowconfigure(0, minsize='30')
        self.rowconfigure(1, minsize='30')
        self.rowconfigure(2, minsize='30')
        self.rowconfigure(3, minsize='30')
        self.rowconfigure(4, minsize='30')
        self.rowconfigure(5, minsize='30')
        self.rowconfigure(6, minsize='30')
        self.rowconfigure(7, minsize='30')
        self.rowconfigure(8, minsize='30')
        self.columnconfigure(0, minsize='0')
        self.columnconfigure(1, minsize='200')
        self.columnconfigure(2, minsize='0')
        self.columnconfigure(3, minsize='200')

    def __init__(self, master=None, data=None):
        ttk.Frame.__init__(self, master)
        self.configure(height='200', padding='20', width='200')
        self.binding = {
            'OID@': StringVar(),
            'flow': StringVar(),
            'speed': StringVar(),
            'start': StringVar(),
            'end': StringVar(),
            'length': StringVar(),
            'diameter': StringVar(),
            'is_active': StringVar(),
        }
        self.read_only_fileds = {'speed', 'flow', 'length'}
        self.createWidgets(data['units'], data['npses'])
        self.initialize_widgets(data['pipe'])

    def initialize_widgets(self, data):
        for k, v in self.binding.iteritems():
            val = data[k] if data.get(k, None) is not None else ''
            if k in self.read_only_fileds and val:
                val = round(val * 100) / 100
            v.set(val)
        self.binding['is_active'].set("True" if data.get('is_active', True) else "False")

    def updateData(self):
        res = {k: v.get() if v.get() != '' else None for k, v in self.binding.iteritems() if k not in self.read_only_fileds}
        res['is_active'] = 1 if res['is_active'] == "True" else 0
        print(json.dumps(res))
        self.quit()

    def updateConnection(self):
        print("update_connection")
        self.quit()


def main():
    root = Tk()
    root.title(LANG.pipe_ui)
    data = json.loads(sys.argv[1]) if len(sys.argv) > 1 else None
    if data is None:
        data = {
            'units': {'length_unit': '', 'flow_unit': '', 'speed_unit': ''},
            'pipe': {},
            'npses': ''
        }
    app = Application(master=root, data=data)
    app.mainloop()
    try:
        root.destroy()
    except:
        pass


def exec_in_sub_process(data):
    file_path = path.abspath(__file__)
    args = json.dumps(data)
    proc = Popen([file_path, args], shell=True, stdout=PIPE, stderr=PIPE, bufsize=1)
    stdoutdata, stderrdata = proc.communicate()
    proc.terminate()
    return stdoutdata.strip()


if __name__ == '__main__':
    main()
