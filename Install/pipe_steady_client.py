# -*- coding: utf-8 -*-
import json
import httplib

from helper import DataStore
from translation import Language
LANG = Language()


def run_pipeline(body, server_address=None):
    server_address = DataStore.instance().server_address if server_address is None else server_address
    headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
    conn = httplib.HTTPConnection(server_address)
    conn.request("POST", "/a/run-pipeline/", json.dumps(body), headers)
    response = conn.getresponse()
    response_content = json.loads(response.read())
    return response_content['id']


def check_pipeline(job_id, server_address=None):
    server_address = DataStore.instance().server_address if server_address is None else server_address
    headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
    conn = httplib.HTTPConnection(server_address)
    conn.request("GET", "/a/check-status/?id={}".format(job_id), headers=headers)
    response = conn.getresponse()
    response_content = json.loads(response.read())
    return response_content


def get_result_pipeline(job_id, server_address=None):
    server_address = DataStore.instance().server_address if server_address is None else server_address
    headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
    conn = httplib.HTTPConnection(server_address)
    conn.request("GET", "/a/get-result/?id={}".format(job_id), headers=headers)
    response = conn.getresponse()
    response_content = json.loads(response.read())
    return response_content


def terminate_job(job_id, server_address=None):
    server_address = DataStore.instance().server_address if server_address is None else server_address
    headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
    conn = httplib.HTTPConnection(server_address)
    conn.request("GET", "/a/terminate-job/?id={}".format(job_id), headers=headers)
    response = conn.getresponse()
    response_content = json.loads(response.read())
    return response_content
