import json
import ttk
from Tkinter import Tk, StringVar
from os import path
from subprocess import Popen, PIPE
from helper import get_unique
from translation import Language
LANG = Language()


class Application(ttk.Frame):
    def createWidgets(self):
        # unique_label widget
        unique_label = ttk.Label(self)
        unique_label.configure(text='Unique:')
        unique_label.grid(column='0', pady='10', row='0')
        # unique_value widget
        unique_value = ttk.Entry(self)
        unique_value.configure(justify='center', state='readonly', textvariable=self.unique, width='70')
        unique_value.grid(column='0', row='1')
        # key_label widget
        key_label = ttk.Label(self)
        key_label.configure(text='Key:')
        key_label.grid(column='0', pady='10', row='2')
        # key_value widget
        key_value = ttk.Entry(self)
        key_value.configure(justify='center', textvariable=self.key, width='70')
        key_value.grid(column='0', row='3')
        # ok widget
        ok = ttk.Button(self, command=self.execute)
        ok.configure(text='OK')
        ok.grid(column='0', pady='10', row='4')
        self.grid(column='0', row='0')

    def __init__(self, master=None):
        ttk.Frame.__init__(self, master)
        self.configure(height='200', padding='20', width='200')
        self.unique = StringVar()
        self.key = StringVar()
        self.unique.set(get_unique())
        self.createWidgets()

    def execute(self):
        print(json.dumps({'key': self.key.get()}))
        self.quit()


def main():
    root = Tk()
    root.title("licence verification")
    app = Application(master=root)
    app.mainloop()
    try:
        root.destroy()
    except:
        pass


def exec_in_sub_process():
    file_path = path.abspath(__file__)
    proc = Popen(file_path, shell=True, stdout=PIPE, stderr=PIPE, bufsize=1)
    stdoutdata, stderrdata = proc.communicate()
    proc.terminate()
    return stdoutdata.strip()


if __name__ == '__main__':
    main()
