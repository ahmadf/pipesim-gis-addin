import sys
import hashlib


def main():
    unique = sys.argv[1] if len(sys.argv) > 1 else ''
    salt = unique + 'PIPESIM_GIS_SECRET_CODE_A'
    key = hashlib.sha224(salt).hexdigest()
    print("the generated key is:")
    print(key)


if __name__ == '__main__':
    main()
